import axios from 'axios';

import { OPEN_SNACKBAR } from './ui';

export const START_GET_AREAS = "@@areas/START_GET";
export const SUCCESS_GET_AREAS = "@@areas/SUCCESS_GET";
export const FAILURE_GET_AREAS = "@@areas/FAILURE_GET";
export const TOGGLE_AREA = "@@area/TOGGLE";

export const getAreas = (active) => (dispatch) => {
  dispatch({ type: START_GET_AREAS });
  axios.get(active ? '/areas?active=true' : '/areas')
    .then((response) => {
      const { data: { data } } = response;
      dispatch({ type: SUCCESS_GET_AREAS, payload: { data } });
    })
    .catch((response) => {
      console.warn(response);
    });
};

export const toggleArea = (id) => (dispatch) => {
  axios.patch(`/areas/${id}`)
    .then((response) => {
      dispatch({ type: TOGGLE_AREA, payload: { id } });
      dispatch({ type: OPEN_SNACKBAR, message: 'Se ha actualizado la información.' });
    })
    .catch();
};
