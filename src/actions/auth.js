import axios from 'axios';

import { replace } from 'connected-react-router';

import { prepareApp } from './core';

export const START_AUTH = "@@auth/START";
export const FAILURE_AUTH = "@@auth/FAILURE";
export const SUCCESS_AUTH = "@@auth/SUCCESS";
export const CLEAR_AUTH = "@@auth/CLEAR";

/**
 * function for auth user
 *
 * @param auth {object} contains email and password
 *
 * @return {function} dispatchEvent
 */
export const startAuth = (auth) => (dispatch) => {
  dispatch({ type: START_AUTH });

  axios.post('/login', auth)
    .then((response) => {
      const { data, status } = response;
      if (status === 200) {
        const { access_token: { type, token }, user } = data;

        /** configure default axios and localStorage */
        axios.defaults.headers['Authorization'] = `${type} ${token}`;
        localStorage.setItem('credentials', `${type} ${token}`);

        dispatch(prepareApp(user));
      }
    })
    .catch((e) => {
      /** TODO: falta en caso que haya ocurrido un error */
      const { response: { data, status } } = e;
      /** Unauthorized */
      if (status === 401) {
        /** the errors fields */
        if (Array.isArray(data)) {
          dispatch({ type: FAILURE_AUTH, payload: { error: data  } });
        } else {
          dispatch({ type: FAILURE_AUTH, payload: { error: data.error.message } });
        }
      }
    });
};

/**
 * function for logout user
 *
 * @return {function} dispatchEvent
 */
export const logout = () => (dispatch) => {
  axios.get('/logout')
    .then((response) => {
      const { status } = response;
      if (status === 204) {
        localStorage.removeItem('credentials');
        dispatch(replace('/'));
        dispatch({ type: CLEAR_AUTH });
      }
    })
    .catch((data) => {
      /** TODO: tratar la exception */
      console.warn(data);
    });
};
