import { replace } from 'connected-react-router';
import axios from 'axios';

import { SUCCESS_AUTH } from './auth';
import { INIT_DRAWER } from './ui';

export const OPEN_APP = '@@core/OPEN_APP';
export const UPDATE_APP_STATUS = '@@core/UPDATE_APP_STATUS';

export const updateAppState = (status) =>
  ({ type: UPDATE_APP_STATUS, payload: { status } });

export const initApp = () => async (dispatch) => {
  dispatch(updateAppState('Iniciando...'));
  await timeout(1000);

  /** get token */
  const credentials = localStorage.getItem('credentials');
  if (credentials) {
    dispatch(updateAppState('Se ha encontrado una sesión.'));
    await timeout(1000);

    dispatch(checkToken(credentials));
  } else {
    dispatch(replace('/'));

    dispatch(openApp());
  }
};

const checkToken = (credentials) => async (dispatch) => {
  dispatch(updateAppState('Comprobando si aun es válido'));
  await timeout(600);

  axios.defaults.headers.common['Authorization'] = credentials;
  axios.get('/me')
    .then(async (response) => {
      const { data, status } = response;

      if (status === 200) {
        /** OK!. the token is available */
        const { user } = data;
        dispatch(prepareApp(user));
        dispatch(openApp());
      }
    })
    .catch(async (data) => {
      const { response: { status } } = data;
      if (status === 401) {
        dispatch(updateAppState('La sesión guardada es invalida.'));
        await timeout(1000);
        localStorage.removeItem('credentials');
        axios.defaults.headers.common['Authorization'] = null;
        dispatch(replace('/'));

        dispatch(openApp());
      }
    });
};

const timeout = (ms) => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

const openApp = () => async (dispatch) => {
  dispatch(updateAppState('Preparando la aplicación.'));
  await timeout(1000);
  dispatch({ type: OPEN_APP });
};

export const prepareApp = (user) => async (dispatch, getState) => {
  /** token valido */
  dispatch({ type: SUCCESS_AUTH, payload: { user } });

  /** init drawer */
  const { role: { id } } = user;
  dispatch({ type: INIT_DRAWER, payload: { id } });

  const state = getState();
  const { router: { location: { pathname } } } = state;

  if (pathname === '/') { /** is root */
    dispatch(replace('/dashboard'));
  }
};
