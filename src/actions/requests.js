import axios from 'axios';
import { replace, goBack } from 'connected-react-router';
import { destroy } from 'redux-form';

import { OPEN_SNACKBAR, CLEAR_STEP, TOGGLE_MODAL } from './ui';

/** reducers functions */
import { getData } from '../reducers/ui/step';

export const START_GET_ACTIONS_PENDING = "@actions_pending/START";
export const SUCCESS_ACTIONS_PENDING = "@@actions_pending/SUCCESS";
export const FAILURE_ACTIONS_PENDIG = "@@actions_pending/FAILURE";

export const START_GET_ACTIONS = "@@actions/START";
export const SUCCESS_GET_ACTIONS = "@@actions/SUCCESS";
export const FAILURE_GET_ACTIONS = "@@actions/FAILURE";
export const ADD_ACTION = "@@actions/ADD";
export const DELETE_ACTION = "@@actions/delete";

export const START_GET_ACTION_INCOMPLETED = "@@action_incompleted/START";
export const SUCCESS_GET_ACTION_INCOMPLETED = "@@action_incompleted/SUCCESS";
export const FAILURE_GET_ACTION_INCOMPLETED = "@@action_incompleted/FAILURE";

export const START_GET_ACTION_VALIDATE = "@@action_validate/START";
export const SUCCESS_GET_ACTION_VALIDATE = "@@action_validate/SUCCESS";
export const FAILURE_GET_ACTION_VALIDATE = "@@action_validate/FAILURE";
export const APPROVE_ACTION_VALIDATE = "@@action_validate/APPROVE";

export const START_GET_REQUESTS = "@@requests/START";
export const SUCCESS_GET_REQUESTS = "@@requets/SUCCESS";
export const FAILURE_GET_REQUESTS = "@@requets/FAILURE";

export const saveRequest = () => (dispatch, getState) => {
  const state = getState();

  const data = getData(state.ui.step, 'newrequest');
  axios.post('/request', data)
    .then((response) => {
      const { data: { data: { folio } } } = response;
      dispatch({ type: OPEN_SNACKBAR, message: `Se ha creado con el folio ${folio}` });
      dispatch({ type: CLEAR_STEP, payload: { name: 'newrequest' } });
      dispatch(replace('/dashboard'));
    })
    .catch((e) => {
      console.warn(e.response);
    });
};

export const getRequestActionsPending = () => (dispatch) => {
  dispatch({ type: START_GET_ACTIONS_PENDING });
  axios.get('/request/actions/pending')
    .then((response) => {
      const { data: { data, code } } = response;
      if (code === 200) {
        dispatch({ type: SUCCESS_ACTIONS_PENDING, payload: { requests: data } });
      }
    })
    .catch();
};

export const addAction = (action, request_id) => (dispatch) => {
  axios.post(`requests/${request_id}/actions`, action)
    .then((response) => {
      const { data: { code, data } } = response;
      if (code === 201) {
        dispatch({ type: OPEN_SNACKBAR, message: `Se ha agrega una nueva accion correctiva.` });
        dispatch({ type: ADD_ACTION, payload: { action: data } });
        dispatch({ type: TOGGLE_MODAL, payload: { modal: 'newaction' } });
        dispatch(destroy('newaction'));
      }
    })
    .catch((e) => {
      console.warn("error " + e.response);
    });
};

export const getActions = (request_id) => (dispatch) => {
  dispatch({ type: START_GET_ACTIONS });
  axios.get(`requests/${request_id}/actions`)
    .then((response) => {
      const { data: { data, code } } = response;

      if (code === 200) {
        dispatch({ type: SUCCESS_GET_ACTIONS, payload: { data } });
      }
    })
    .catch((e) => {
      console.warn("error " + e.response);
    });
};

export const deleteAction = (id) => (dispatch) => {
  axios.delete(`actions/${id}`)
    .then((response) => {
      const { data: { code } } = response;
      if (code === 200) {
        dispatch({ type: DELETE_ACTION, payload: { id } });
        dispatch({ type: OPEN_SNACKBAR, message: 'Se ha borrado la acción correctiva' });
      }
    })
    .catch();
};

export const closeActions = (id) => (dispatch) => {
  axios.get(`requests/${id}/closeActions`)
    .then((response) => {
      const { data: { code } } = response;

      if (code === 200) {
        dispatch(goBack());
        dispatch({ type: OPEN_SNACKBAR, message: 'Se ha cerrado con éxito la captura de las acciones' });
      }
    });
};

export const getActionsIncompleted = () => (dispatch) => {
  dispatch({ type: START_GET_ACTION_INCOMPLETED });
  axios.get('actions')
    .then((response) => {
      const { data: { data, code } } = response;

      if (code === 200) {
        dispatch({ type: SUCCESS_GET_ACTION_INCOMPLETED, payload: { data } });
      }
    })
    .catch((e) => {
      console.warn("error: " + e.response);
    });
};

export const saveEvidence = (evidence, action_id) => (dispatch) => {
  let body = new FormData();
  body.append('results', evidence.results);
  body.append('observations', evidence.observations);
  body.append('evidence', evidence.evidence[0]);

  axios.post(`actions/${action_id}/evidences`, body, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
    .then((response) => {
      const { data: { code } } = response;

      if(code === 200) {
        dispatch(goBack());
        dispatch({ type: OPEN_SNACKBAR, message: 'Se ha enviando la evidencia con éxito.' });
      }
    })
    .catch((e) => {
      console.warn(e.response);
    });
};

export const getActionsToBeEvaluated = () => (dispatch) => {
  dispatch({ type: FAILURE_GET_ACTION_VALIDATE });
  axios.get('/actions/validate')
    .then((response) => {
      const { data: { data, code } } = response;

      if (code === 200) {
        dispatch({ type: SUCCESS_GET_ACTION_VALIDATE, payload: { data } });
      }
    })
    .catch((e) => {
      console.warn(e.response);
    });
};

export const approveAction = (id) => (dispatch) => {
  axios.get(`actions/${id}/approve`)
    .then((response) => {
      const { data: { code } } = response;

      if (code === 200) {
        dispatch({ type: APPROVE_ACTION_VALIDATE, payload: { id } });

        dispatch({ type: OPEN_SNACKBAR, message: 'Listo!.' });
      }
    })
    .catch((e) => {
      console.warn(e.response);
    });
};

export const completedRequest = (id) => (dispatch) => {
  axios.get(`request/${id}/completed`)
    .then((response) => {
      const { data: { code } } = response;

      if (code === 200) {
        dispatch(goBack());
      }
    })
    .catch((e) => {
      console.warn(e.response);
    });
};

export const getRequests = () => (dispatch) => {
  dispatch({ type: START_GET_REQUESTS });

  axios.get('requests?completed=true')
    .then((response) => {
      const { data: { data, code } } = response;

      if (code === 200) {
        dispatch({ type: SUCCESS_GET_REQUESTS, payload: { data } });
      }
    })
    .catch((e) => {
      console.warn(e.response);
    });
};
