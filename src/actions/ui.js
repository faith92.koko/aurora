import { push } from 'connected-react-router';

export const TOGGLE_DRAWER = "@@ui/TOGGLE_DRAWER";
export const TOGGLE_OPTION_MENU = "@@ui/TOGGLE_OPTION_MENU";
export const INIT_DRAWER = "@@ui/INIT_DRAWER";

/** snackbar */
export const OPEN_SNACKBAR = "@@ui/OPEN_SNACKBAR";
export const CLOSE_SNACKBAR = "@@ui/CLOSE_SNACKBAR";

/** step */
export const REGISTER_STEP = "@@step/REGISTER";
export const COMPLETE_STEP = "@@step/COMPLETE";
export const BACK_STEP = "@@step/BACK";
export const CLEAR_STEP = "@@step/CLEAR";

/** modals */
export const REGISTER_MODAL = "@@modal/REGISTER";
export const TOGGLE_MODAL = "@@modal/TOGGLE";

/**
 * toggle for the drawer, when trigger a this function
 * the drawer change the prop isOpen.
 */
export const toggleDrawer = () =>
  ({ type: TOGGLE_DRAWER });

/**
 * toggle for the option
 * @param {string} id id option
 */
export const toggleOptionMenu = (id) =>
  ({ type: TOGGLE_OPTION_MENU, payload: { id } });

/**
 * function for open a option
 * @param {string} path path
 */
export const openOption = (path) => (dispatch) => {
  dispatch(toggleDrawer());
  dispatch(push(path));
};

export const closeSnackbar = () =>
  ({ type: CLOSE_SNACKBAR });

/** actions for step component */

export const registerStep = (name, steps) =>
  ({ type: REGISTER_STEP, payload: { name, steps } });

export const completeStep = (name, data) =>
  ({ type: COMPLETE_STEP, payload: { name, data } });

export const backStep = (name) =>
  ({ type: BACK_STEP, payload: { name } });

export const registerModal = (modal) =>
  ({ type: REGISTER_MODAL, payload: { modal } });

export const toggleModal = (modal) =>
  ({ type: TOGGLE_MODAL, payload: { modal } });
