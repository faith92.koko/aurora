import axios from 'axios';
import { goBack } from 'connected-react-router';

import{ OPEN_SNACKBAR } from './ui';

/** create user */
export const START_CREATE_USER = "@@users/START_CREATE";
export const SUCCESS_CREATE_USER = "@@users/SUCCESS_CREATE";
export const FAILURE_CREATE_USER = "@@users/FAILURE_CREATE";

/** get users */
export const START_GET_USERS = "@@users/START_GET";
export const SUCCESS_GET_USERS = "@@users/SUCCESS_GET";
export const FAILURE_GET_USERS = "@@users/FAILURE_GET";

/** update user */
export const START_UPDATE_USER = "@@users/START_UPDATE";
export const SUCCESS_UPDATE_USER = "@@users/SUCCESS_UPDATE";
export const FAILURE_UPDATE_USER = "@@users/FAILURE_UPDATE";

/**
 * function to create a new user
 *
 * @params {object} user
 *
 * @return {function} dispatchEvent
 */
export const createUser = (user) => (dispatch) => {
  axios.post('/users', user)
    .then((response) => {
      dispatch(goBack());
      dispatch({ type: OPEN_SNACKBAR, message: 'Usuario creado' });
    })
    .catch((e) => {
      console.warn(e.response);
    });
};

/**
 * function to get all users
 *
 * @params {function} dispatchEvent
 */
export const getUsers = () => (dispatch) => {
  dispatch({ type: START_GET_USERS });

  axios.get('/users')
    .then((response) => {
      const { data: { users, status } } = response;
      if (status === 200) {
        dispatch({ type: SUCCESS_GET_USERS, payload: { data: users } });
      }
    })
    .catch((e) => {
      console.warn(e);
    });
};

/**
 * function to update user by id
 *
 * @param {object} user user data
 *
 * @return {function} dispatchEvent
 */
export const updateUser = (user) => (dispatch) => {
  dispatch({ type: START_UPDATE_USER });

  axios.patch('/users', user)
    .then((response) => {
      const { data: { status } } = response;
      if (status) {
        dispatch({ type: SUCCESS_UPDATE_USER, payload: { data: user } });
        dispatch({ type: OPEN_SNACKBAR, message: 'Información actualizada.' });
      }
    }).catch((e) => {
      console.warn(e.response);
    });
};
