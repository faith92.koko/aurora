import React from 'react';
import { Field } from 'redux-form';
import moment from 'moment';

import {
  Typography,
  Divider,
  Paper,
  Button
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

/** components */
import RenderFileField from './commons/RenderFileField';
import RenderField from './commons/RenderField';

const styles = {
  container: {
    padding: 12
  }
};

const Action = ({ classes, handleSubmit, action: { scheduled_date } }) => (
  <div className="container">
    <div className="row justify-content-md-center">
      <div className="col-12 col-sm-12 col-md-8 col-lg-8">
        <Paper square>
          <div className={classes.container}>
            <Typography variant="h5">
              Capture los resultados
            </Typography>
            <Typography variant="subtitle2">
              Capture los resultados de la acción correctiva.
            </Typography>
          </div>
          <Divider/>
          <div className={classes.container}>
            <form onSubmit={handleSubmit}>
              <Field
                name="results"
                component={RenderField}
                type="text"
                label="Resultados"
                hint = "Introduzca los resultados obtenidos"
              />
              {
                moment().diff(scheduled_date, 'days') > 0 && (
                  <div style={{ marginTop: 6 }}>
                    <Field
                      name="observations"
                      component={RenderField}
                      type="text"
                      label="Motivo del retraso"
                      hint = "Introduzca los motivos del retraso"
                    />
                  </div>
                )
              }
              <div style={{ marginTop: 6 }}>
                <Field
                  name="evidence"
                  component={RenderFileField}
                />
              </div>
              <Button variant="outlined" color="primary" type="submit" style={{ marginTop: 6 }}>
                enviar resultado
              </Button>
            </form>
          </div>
        </Paper>
      </div>
    </div>
  </div>
);

export default withStyles(styles)(Action);
