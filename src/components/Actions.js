import React from 'react';

import {
  Paper,
  Typography,
  Divider
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

/** components */
import Item from '../containers/requests/Item';

const styles = {
  wraper: {
    padding: 12
  },
  bodylist: {
    paddingTop: 6,
    paddingBottom: 6
  },
  noitems: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  noitemstext: {
    color: '#757575'
  }
};

const Actions = ({ classes, requests: { isFetching, data } }) => (
  <div className="container">
    <div className="row justify-content-md-center">
      <div className="col-12 col-sm-12 col-md-8 col-lg-8">
        <Paper square>
          <div className={classes.wraper}>
            <Typography variant="h5">
              Solicitudes pendientes
            </Typography>
            <Typography variant="subtitle2">
              Solicitudes pendientes por capturar las acciones correctivas.
            </Typography>
          </div>
          <Divider/>
          <div className={classes.bodylist}>
            {
              data.length === 0 && (
                <div className={classes.noitems}>
                  <Typography className={classes.noitemstext}>
                    Parece ser que no solicitudes pendientes
                  </Typography>
                </div>
              )
            }
            {
              data.map((request) => <Item key={request.id} {...request}/>)
            }
          </div>
        </Paper>
      </div>
    </div>
  </div>
);

export default withStyles(styles)(Actions);
