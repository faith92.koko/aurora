import React from 'react';

import {
  Paper,
  Typography,
  Divider
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

/** components */
import Modal from '../containers/requests/Modal';
import Action from '../containers/requests/Action';

const styles = {
  container: {
    padding: 12
  },
  buttonAdd: {
    marginTop: 6,
    borderRadius: 40,
    border: 'solid #233D7B 1.2px',
    display: 'flex',
    justifyContent: 'center',
    cursor: 'pointer'
  },
  textButton: {
    color: '#233D7B'
  },
  list: {
    paddingTop: 6,
    paddingBottom: 6
  },
  noitems: {
    color: '#757575'
  }
};

const ActionsPanel = ({ classes, request: { folio, description, request_type: { name }, created_at }, newAction, request_id, actions: { isFetching, data }, closeActions }) => (
  <div className="container">
    <div className="row">
      <div className="col-10 col-sm-12 col-md-8 col-lg-8">
        <Paper square>
          <div className={classes.container}>
            <Typography variant="h5">
              Acciones correctivas
            </Typography>
            <Typography>
              Captura de acciones correctivas
            </Typography>
          </div>
          <Divider/>
          <div className={classes.list}>
            {
              isFetching
                ? <span className={classes.container}>Descargando información</span>
                : data.length === 0
                  ? (
                    <div style={{ display: 'flex', justifyContent: 'center', 'alignItems': 'center' }}>
                      <Typography className={classes.noitems}>No hay acciones, capture una nueva presionando el botón de abajo.</Typography>
                    </div>
                  )
                  : data.map((action) => <Action key={action.id} {...action}/>)
            }
          </div>
        </Paper>
        <Paper square className={classes.buttonAdd} onClick={newAction}>
          <div className={classes.container}>
            <Typography className={classes.textButton}>
              Agregar nueva acción correctiva
            </Typography>
          </div>
        </Paper>
      </div>
      <div className="col-2 col-sm-12 col-md-4 col-lg-4">
        <Paper square>
          <div className={classes.container}>
            <Typography variant="h5">
              No conformidad
            </Typography>
            <Typography>
              Resumen de la solicitud
            </Typography>
          </div>
          <Divider/>
          <div className={classes.container}>
            <Typography>
              <strong>Fecha de creación: </strong> {created_at}
            </Typography>
            <Typography>
              <strong>Folio: </strong> {folio}
            </Typography>
            <Typography>
              <strong>Tipo: </strong> {name}
            </Typography>
            <Typography>
              <strong>Descripción: </strong> {description}
            </Typography>
          </div>
        </Paper>
        {
          data.length > 0 && (
            <Paper square className={classes.buttonAdd} onClick={closeActions}>
              <div className={classes.container}>
                <Typography className={classes.textButton}>
                  Cerrar captura de acciones
                </Typography>
              </div>
            </Paper>
          )
        }
      </div>
    </div>
    <Modal request_id={request_id}/>
  </div>
);

ActionsPanel.defaultProps = {
  request: {
    folio: 'sgc/2018-001',
    description: 'Se generó la no conformidad cuando los alumnos se fueron a quejar.',
    request_type: 'Queja'
  }
};

export default withStyles(styles)(ActionsPanel);
