import React from 'react';

import {
  Paper,
  Typography,
  Divider
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

/** components */
import ActionValidate from './requests/ActionValidate';

const styles = {
  container: {
    padding: 12
  },
  list: {
    paddingTop: 6,
    paddingBottom: 6
  },
  noitems: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  noitemstext: {
    color: '#757575'
  }
};

const ActionsValidate = ({ actions: { data }, classes, go }) => (
  <div className="container">
    <div className="row justify-content-md-center">
      <div className="col-12 col-sm-12 col-md-8 col-lg-8">
        <Paper square>
          <div className={classes.container}>
            <Typography variant="h5">
              Evaluar Acciones
            </Typography>
            <Typography variant="subtitle2">
              Evalue las acciones que fueron implementadas
            </Typography>
          </div>
          <Divider/>
          <div className={classes.list}>
            {
              data.length === 0 && (
                <div className={classes.noitems}>
                  <Typography className={classes.noitemstext}>
                    Parecer ser que no tiene evaluaciones por evaluar
                  </Typography>
                </div>
              )
            }
            {
              data.map((action) => <ActionValidate key={action.id} {...action} go={() => go(action.id)}/>)
            }
          </div>
        </Paper>
      </div>
    </div>
  </div>
);

export default withStyles(styles)(ActionsValidate);
