import React from 'react';

import { Route, Switch } from 'react-router';

import { withStyles } from '@material-ui/core/styles';

import Header from '../containers/Header';
import Login from '../containers/Login';
import Dashboard from './Dashboard';
import Footer from './Footer';
import Menu from '../containers/Menu';
import FullPage from './FullPage';
import Snackbar from '../containers/ui/Snackbar';

import CreateUser from '../containers/users/CreateUser';
import UpdateUser from '../containers/users/UpdateUser';
import List from '../containers/users/List';
import CreateRequest from '../containers/requests/Create';
import Areas from '../containers/Areas';
import Actions from '../containers/Actions';
import ActionsPanel from '../containers/ActionsPanel';
import ResultsOfActions from '../containers/ResultsOfActions';
import Action from '../containers/Action';
import ActionsValidate from '../containers/ActionsValidate';
import Validate from '../containers/Validate';
import Requests from '../containers/Requests';
import Resume from '../containers/Resume';

const styles = {
  container: {
    paddingTop: 12,
    paddingBottom: 12
  }
};

const App = ({ app: { isReady, ...props }, classes }) =>
  isReady
    ? (
      <div>
        <Snackbar/>
        <Header/>
        <Menu/>
        <div className={classes.container}>
          <Switch>
            <Route exact path="/" component={Login}/>
            <Route path="/dashboard" component={Dashboard}/>
            <Route exact path="/usuarios/crear" component={CreateUser}/>
            <Route exact path="/usuarios" component={List}/>
            <Route exact path="/usuarios/:id" component={UpdateUser}/>
            <Route exact path="/solicitudes" component={Requests}/>
            <Route exact path="/solicitudes/no-conformidad" component={CreateRequest}/>
            <Route exact path="/configuracion/areas-responsables" component={Areas}/>
            <Route exact path="/acciones/capturar" component={Actions}/>
            <Route exact path="/acciones/capturar/:id" component={ActionsPanel}/>
            <Route exact path="/acciones/evaluar" component={ActionsValidate}/>
            <Route exact path="/acciones/evaluar/:id" component={Validate}/>
            <Route exact path="/acciones" component={ResultsOfActions}/>
            <Route exact path="/acciones/:id" component={Action}/>
            <Route exact path="/resumen" component={Resume}/>
          </Switch>
        </div>
        <Footer/>
      </div>
    )
    : <FullPage {...props}/>;

export default withStyles(styles)(App);
