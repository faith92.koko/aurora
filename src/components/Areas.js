import React from 'react';

import {
  Paper,
  Typography,
  Divider,
  Switch
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

const Area = ({ name, active, toggleArea }) => (
  <div style={{ padding: 12, display: 'flex', flexDirection: 'row' }}>
    <div style={{ flex: 1, display: 'flex', alignItems: 'center' }}>
      <span>{name}</span>
    </div>
    <Switch checked={active} onChange={toggleArea}/>
  </div>
);

const styles = {
  wraper: {
    padding: 12
  },
  bodylist: {
    paddingTop: 6,
    paddingBottom: 6
  }
};

const Areas = ({ areas: { data, id }, classes, toggleArea }) => (
  <div className="container">
    <div className="row justify-content-md-center">
      <div className="col-12 col-sm-12 col-md-8 col-lg-6">
        <Paper square>
          <div className={classes.wraper}>
            <Typography variant="h5">
              Areas Reponsable
            </Typography>
            <Typography variant="subtitle2">
              Active / Desactive las Areas Responsables.
            </Typography>
          </div>
          <Divider/>
          <div className={classes.bodylist}>
            {
              data.map((area) =>
                <Area key={area.id} {...area} toggleArea={() => toggleArea(area.id)}/>)
            }
          </div>
        </Paper>
      </div>
    </div>
  </div>
);

export default withStyles(styles)(Areas);
