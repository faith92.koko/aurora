import React from 'react';

const Footer = ({ classes }) => (
  <footer className="footer">
    <span className="text-center font-italic" style={{ fontSize: 12 , color: '#F5F5F5' }}>
      &copy; Copyright 2018 | Todos los derechos reservados
    </span>
  </footer>
);

export default Footer;
