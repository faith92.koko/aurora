import React from 'react';

import {
  CircularProgress,
  Typography
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

const styles = {
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    backgroundImage: `url(${require('../images/tecnm-grey.png')})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundPositionX: 0,
    backgroundSize: '20%',
    backgroundColor: '#FAFAFA'
  }
};

const FullPage = ({ classes, status }) => (
  <div className={classes.container}>
    <CircularProgress className={classes.progress} color="secondary" size={60} thickness={1}/>
    <Typography variant="h6">
      Espere un momento.
    </Typography>
    <Typography variant="subtitle1">
      {status}
    </Typography>
  </div>
);

FullPage.defaultProps = {
  status: 'required the field {status}',
  message: 'required the field {message}'
};

export default withStyles(styles)(FullPage);
