import React from 'react';
import PropTypes from 'prop-types';

import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Button
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import MenuIcon from '@material-ui/icons/Menu';

const styles = {
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  appbar: {
    backgroundColor: '#233D7B'
  },
  images: {
    width: '100%',
    height: 72,
    display: 'flex',
    justifyContent: 'space-between'
  },
  image: {
    display: 'flex',
    alignItems: 'center'
  }
};

const Header = ({ classes, isAuth, logout, toggle }) => (
  <div className={classes.root}>
    <div className={classes.images}>
      <div className={classes.image}>
        <img
          className={classes.image}
          height="100%"
          src={require('../images/tecnm-cortado-grey.png')}
          alt="imagen del tecnm"
        />
      </div>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <img
          height="90%"
          src={require('../images/tecnm-original.png')}
          alt="logotipo del tecnm"
        />
      </div>
    </div>
    <AppBar position="relative" className={classes.appbar}>
      <Toolbar>
        {
          isAuth && (
            <IconButton
              className={classes.menuButton} color="inherit" aria-label="Menu"
              onClick={toggle}
            >
              <MenuIcon/>
            </IconButton>
          )
        }
        <Typography variant="h6" color="inherit" className={`${classes.grow} d-inline-block text-truncate`}>
          Sistema Gestión de Calidad
        </Typography>
        {
          isAuth && (
            <div>
              <Button onClick={logout} color="inherit" className="d-inline-block text-truncate">
                cerrar sesión
              </Button>
            </div>
          )
        }
      </Toolbar>
    </AppBar>
  </div>
);

Header.defaultProps = {
  isAuth: false,
  logout: () => { console.warn('required the function {logout}'); },
  toggle: () => { console.warn('required the function {toggle}'); }
};

Header.propTypes = {
  classes: PropTypes.object,
  isAuth: PropTypes.bool.isRequired,
  logout: PropTypes.func.isRequired,
  toggle: PropTypes.func.isRequired
};

export default withStyles(styles)(Header);
