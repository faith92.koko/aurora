import React from 'react';
import PropTypes from 'prop-types';

import { Field } from 'redux-form';

import {
  Paper,
  Typography,
  Button
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

import RenderField from './commons/RenderField';

const styles = {
  paper: {
    padding: 20,
    position: 'relative',
    marginBottom: 60
  },
  fields: {
    marginTop: 6
  },
  button: {
    marginTop: 20
  },
  errors: {
    marginBottom: 6,
    backgroundColor: '#F44336',
    color: '#ffffff',
    paddingLeft: 12
  }
};

const Login = ({ classes, handleSubmit, isLogging, errors }) => (
  <div className="container">
    {
      errors && (
        <div className="row justify-content-md-center">
          <div className="col-12 col-sm-12 col-md-8 col-lg-6">
            <Paper className={classes.errors} square>
              <strong>Ups!.</strong>
              <Typography color="inherit">
                {Array.isArray(errors) ? errors[0].message : errors}
              </Typography>
            </Paper>
          </div>
        </div>
      )
    }
    <div className="row justify-content-md-center">
      <div className="col-12 col-sm-12 col-md-8 col-lg-6">
        <Paper square className={classes.paper}>
          <Typography variant="h6">
            Iniciar Sesión
          </Typography>
          <Typography variant="subtitle2">
            Comienza una sesión con tu cuenta
          </Typography>
          <div className={classes.fields}>
            <form onSubmit={handleSubmit}>
              <Field
                name="email"
                component={RenderField}
                type="email"
                label="Correo electrónico"
                hint = "Introduzca su correo electrónico"
              />
              <Field
                name="password"
                component={RenderField}
                type="password"
                label="Contraseña"
                hint = "Introduzca su contraseña"
              />
              <div className="d-flex justify-content-end">
                <Button
                  variant="outlined"
                  color="secondary"
                  className={classes.button}
                  type="submit"
                  disabled={isLogging}
                >
                  iniciar
                </Button>
              </div>
            </form>
          </div>
        </Paper>
      </div>
    </div>
  </div>
);

Login.defaultProps = {
  handleSubmit: () => { console.warn('required the function {handleSubmit}'); },
  isLogging: false,
  errors: null
};

Login.propTypes ={
  classes: PropTypes.object,
  handleSubmit: PropTypes.func.isRequired,
  isLogging: PropTypes.bool.isRequired
};

export default withStyles(styles)(Login);
