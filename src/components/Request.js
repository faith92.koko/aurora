import React from 'react';

import {
  Typography
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

const styles = {
  container: {
    padding: 12,
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: '#F5F5F5'
    }
  }
};

const Request = ({ classes, folio, created_at, request_type: { name } }) => (
  <div className={classes.container}>
    <Typography>
      <strong>Fecha de inicio: </strong>{created_at}
    </Typography>
    <Typography>
      <strong>Folio: </strong>{folio}
    </Typography>
    <Typography>
      <strong>Tipo de solicitud: </strong>{name}
    </Typography>
  </div>
);

export default withStyles(styles)(Request);
