import React from 'react';

import {
  Paper,
  Typography,
  Divider
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

import Request from './Request';

const styles = {
  container: {
    padding: 12
  }
};

const Requests = ({ classes, requests }) => (
  <div className="container">
    <div className="row justify-content-md-center">
      <div className="col-12 col-sm-12 col-md-8 col-lg-8">
        <Paper square>
          <div className={classes.container}>
            <Typography variant="h5">
              Solicitudes Cerradas
            </Typography>
            <Typography variant="subtitle2">
              Consulte la información de las solicitudes cerradas
            </Typography>
          </div>
          <Divider/>
          <div>
            {
              requests.map((request) => <Request key={request.id} {...request}/>)
            }
          </div>
        </Paper>
      </div>
    </div>
  </div>
);

export default withStyles(styles)(Requests);
