import React from 'react';

import {
  Paper,
  Typography,
  Divider
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

/** components */
import ActionNoCompleted from './requests/ActionNoCompleted';

const styles = {
  wraper: {
    padding: 12
  },
  list: {
    paddingTop: 6,
    paddingBottom: 6
  },
  noitems: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  noitemstext: {
    color: '#757575'
  }
};

const ResultsOfActions = ({ classes, actions, push }) => (
  <div className="container">
    <div className="row justify-content-md-center">
      <div className="col-12 col-sm-12 col-md-8 col-lg-8">
        <Paper square>
          <div className={classes.wraper}>
            <Typography variant="h5">
              Acciones por completar
            </Typography>
            <Typography variant="subtitle2">
              Capture los resultados de implementar las siguientes acciones
            </Typography>
          </div>
          <Divider/>
          <div className={classes.list}>
            {
              actions.length === 0 && (
                <div className={classes.noitems}>
                  <Typography className={classes.noitemstext}>
                    Parece ser que no tiene acciones por completar
                  </Typography>
                </div>
              )
            }
            {
              actions.map((action) => <ActionNoCompleted key={action.id} {...action} onClick={() => push(`acciones/${action.id}`)}/>)
            }
          </div>
        </Paper>
      </div>
    </div>
  </div>
);

export default withStyles(styles)(ResultsOfActions);
