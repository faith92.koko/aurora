import React, { PureComponent } from 'react';

import { Typography } from '@material-ui/core';

import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from  'recharts';

class Resume extends PureComponent {
  render () {
    const { requests, areas } = this.props;

    let sgc = 0;
    let fsgc = 0;
    let sga = 0;
    let fsga = 0;

    const rCompleted = requests.filter((r) => r.completed);

    requests.forEach((request) => {
      if (request.origen === 'sgc') {
        if (request.completed) {
          sgc++;
        } else {
          fsgc++;
        }
      }

      if (request.origen === 'sga') {
        if (request.completed) {
          sga++;
        } else {
          fsga++;
        }
      }

    });

    const data = [
      { name: 'SGC', cantidad: sgc, faltantes: fsgc },
      { name: 'SGA', cantidad: sga, faltantes: fsga }
    ];

    let quejas = 0;
    let q2 = 0;
    let auditoria = 0;
    let a2 = 0;
    let incumplimientorequisitos = 0;
    let i2 = 0;
    let eficaciaprocesos = 0;
    let e2 = 0;
    let incumplimiento = 0;
    let in2 = 0;
    let otro = 0;
    let o2 = 0;

    requests.forEach((request) => {
      if (request.request_type.id === 1) {
        if (request.completed) {
          quejas++;
        } else {
          q2++;
        }
      }

      if (request.request_type.id === 2) {
        if (request.completed) {
          auditoria++;
        } else {
          a2++;
        }
      }

      if (request.request_type.id === 3) {
        if (request.completed) {
          incumplimientorequisitos++;
        } else {
          i2++;
        }
      }

      if (request.request_type.id === 4) {
        if (request.completed) {
          eficaciaprocesos++;
        } else {
          e2++;
        }
      }

      if (request.request_type.id === 5) {
        if (request.completed) {
          incumplimiento++;
        } else {
          in2++;
        }
      }

      if (request.request_type.id === 6) {
        if (request.completed) {
          otro++;
        } else {
          o2++;
        }
      }
    });

    const data2 = [
      { name: 'Quejas', cantidad: quejas, faltantes: q2 },
      { name: 'Auditoria', cantidad: auditoria, faltantes: a2 },
      { name: 'Incumplimiento de requisitos', cantidad: incumplimientorequisitos, faltantes: i2 },
      { name: 'Eficacia / desempeño de procesos', cantidad: eficaciaprocesos, faltantes: e2 },
      { name: 'Incumplimiento objetivos', cantidad: incumplimiento, faltantes: in2 },
      { name: 'Otro', cantidad: otro, faltantes: o2 }
    ];

    let data3 = [];

    areas.forEach((area) => {
      data3 = [
        ...data3,
        { id: area.id, name: area.name, cantidad: 0 }
      ];
    });

    rCompleted.forEach((request) => {
      data3.map((area) => {
        return request.responsible_area_id === area.id ? { ...area, cantidad: area.cantidad++ } : area ;
      });
    });

    /** calcular por proceso estratégico */
    let data4 = [ {
      id: 1,
      name: 'Académico',
      total: 0,
      faltantes: 0
    }, {
      id: 2,
      name: 'Vinculación',
      total: 0,
      faltantes: 0
    }, {
      id: 3,
      name: 'Planeación',
      total: 0,
      faltantes: 0
    }, {
      id: 4,
      name: 'Admon. de R.',
      total: 0,
      faltantes: 0
    }, {
      id: 5,
      name: 'Calidad',
      total: 0,
      faltantes: 0
    } ];

    data3.forEach((data) => {
      if ([ 5,6,7,8 ].includes(data.id)) {
        data4[0].total = data4[0].total + data.cantidad;
      } else if ([ 14,16,17,19 ].includes(data.id)) {
        data4[1].total = data4[1].total + data.cantidad;
      } else if ([ 15, 18 ].includes(data.id)) {
        data4[2].total = data4[2].total + data.cantidad;
      } else if ([ 9, 10, 11, 12, 13 ].includes(data.id)) {
        data4[3].total = data4[3].total + data.cantidad;
      } else {
        data4[4].total = data4[4].total + data.cantidad;
      }
    });

    return (
      <div style={{ marginTop: 20 }}>
        <div className="container">
          <div className="row justify-content-md-center">
            <div className="col-12 col-sm-12 col-md-8 col-lg-8">
              <Typography variant="h5" style={{ marginBottom: 6 }}>
                Trámites capturados
              </Typography>
              <BarChart width={600} height={300} data={data} margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                <CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="name"/>
                <YAxis label={{ value: 'RAC\'s', angle: -90, position: 'insideLeft' }} allowDecimals={false}/>
                <Tooltip/>
                <Legend/>
                <Bar dataKey="cantidad" stackId="a" fill="#233D7B" name="No de RAC's"/>
                <Bar dataKey="faltantes" stackId="a" fill="#82ca9d" name="Por terminar"/>
              </BarChart>
            </div>
          </div>

          <div className="row justify-content-md-center">
            <div className="col-12 col-sm-12 col-md-8 col-lg-8">
              <Typography variant="h5" style={{ marginBottom: 6 }}>
                Trámites completados por proceso estrátegico
              </Typography>
              <BarChart width={600} height={300} data={data4} margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                <CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="name"/>
                <YAxis label={{ value: 'RAC\'s', angle: -90, position: 'insideLeft' }} allowDecimals={false}/>
                <Tooltip/>
                <Legend/>
                <Bar dataKey="total" fill="#233D7B" name="No de RAC's"/>
              </BarChart>
            </div>
          </div>

          <div className="row justify-content-md-center">
            <div className="col-12 col-sm-12 col-md-8 col-lg-8">
              <Typography variant="h5" style={{ marginBottom: 6 }}>
                Trámites capturados por tipo
              </Typography>
              <BarChart width={600} height={300} data={data2} margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                <CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="name"/>
                <YAxis label={{ value: 'RAC\'s', angle: -90, position: 'insideLeft' }} allowDecimals={false}/>
                <Tooltip/>
                <Legend/>
                <Bar dataKey="cantidad" stackId="a" fill="#233D7B" name="No de RAC's"/>
                <Bar dataKey="faltantes" stackId="a" fill="#82ca9d" name="Por terminar"/>
              </BarChart>
              <Typography variant="caption" style={{ marginBottom: 6 }}>
                Nota: Se muestran los trámites de no conformidades totales por tipo de origen
              </Typography>
            </div>
          </div>

          <div className="row justify-content-md-center">
            <div className="col-12 col-sm-12 col-md-12 col-lg-12">
              <Typography variant="h5" style={{ marginBottom: 6 }}>
                Trámites por área responsable
              </Typography>
              <BarChart width={1100} height={300} data={data3.filter((data) => data.cantidad !== 0)} margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                <CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="name" style={{ fontSize: 12 }} interval={0}/>
                <YAxis label={{ value: 'RAC\'s', angle: -90, position: 'insideLeft' }} allowDecimals={false}/>
                <Tooltip/>
                <Legend/>
                <Bar dataKey="cantidad" fill="#233D7B" name="No de RAC's"/>
              </BarChart>
              <Typography variant="caption" style={{ marginBottom: 6 }}>
                Nota: Solo se muestran las áreas responsables con no conformidades capturadas
              </Typography>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Resume;
