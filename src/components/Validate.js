import React from 'react';

import {
  Paper,
  Typography,
  Button,
  Divider
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

/** components */
import ItemValidate from './requests/ItemValidate';

const styles = {
  container: {
    padding: 12
  }
};

const Validate = ({ actions, approveAction, completed, classes }) => (
  <div className="container">
    <div className="row justify-content-md-center">
      <div className="col-12 col-sm-12 col-md-8 col-lg-8">
        <Paper square>
          <div className={classes.container}>
            <Typography variant="h5">
              Resultados de las acciones
            </Typography>
            <Typography variant="subtitle2">
              Evalue los resultados de las acciones implementar
            </Typography>
          </div>
          <Divider/>
          {
            actions.length === 0 && <Button onClick={completed} fullWidth color="primary">Terminar la revisión</Button>
          }
        </Paper>
      </div>
      {
        actions.map((action) => <ItemValidate key={action.id} {...action} approve={approveAction}/>)
      }
    </div>
  </div>
);

Validate.defaultProps = {
  actions: []
};

export default withStyles(styles)(Validate);
