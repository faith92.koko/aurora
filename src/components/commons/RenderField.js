import React from 'react';

import { TextField } from '@material-ui/core';

const RenderField = ({ input, label, type, meta: { touched, error, warning }, hint, onKeyDown, ref, autoFocus, multiline, rows, hidden }) => (
  <div style={{ paddingTop: 6 }}>
    <TextField
      helperText={touched && error}
      error={touched && error ? true : false}
      autoComplete="off"
      label={label}
      autoFocus={autoFocus}
      placeholder={hint}
      fullWidth
      type={type}
      onKeyDown={onKeyDown}
      multiline={multiline}
      rows={rows}
      InputLabelProps={{
        shrink: true
      }}
      hidden={hidden}
      { ...input }
    />
  </div>
);

export default RenderField;
