import React from 'react';
import Dropzone from 'react-dropzone';

import { Typography } from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

const styles = {
  dropzone: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    border: 'solid #E0E0E0 0.7px', padding: 12
  },
  error: {
    color: '#f44336',
    fontSize: '0.75rem'
  },
  filemessage: {
    marginTop: 6
  }
};

const RenderFileField = (field) => (
  <div>
    <Dropzone
      accept="image/png, image/jpeg"
      className={field.classes.container}
      name={field.name}
      onDrop={( filesToUpload, e ) => field.input.onChange(filesToUpload)}
      multiple={false}
    >
      <div className={field.classes.dropzone}>
        <Typography>Presione clic sobre esta area o arrastre una evidencia (*.img)</Typography>
      </div>
    </Dropzone>
    {
      field.meta.touched &&
        field.meta.error &&
          <Typography className={field.classes.error}>
            {field.meta.error}
          </Typography>
    }
    {
      field.input.value && Array.isArray(field.input.value) && (
        <Typography className={field.classes.filemessage}>
          <strong>Evidencia adjuntada: </strong>{field.input.value[0].name}
        </Typography>
      )
    }
  </div>
);

export default withStyles(styles)(RenderFileField);
