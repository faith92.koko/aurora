import React from 'react';

import {
  RadioGroup
} from '@material-ui/core';

const RenderRadioGroup = ({ input, children }) => (
  <RadioGroup
    {...input}
    value={input.value}
    onChange={(event, value) => input.onChange(value)}
  >
    {
      children
    }
  </RadioGroup>
);

export default RenderRadioGroup;
