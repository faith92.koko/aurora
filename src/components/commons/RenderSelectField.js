import React from 'react';

import { TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  }
});

const RenderSelectField = ({ classes, input, elements, criteria, placeholder, meta: { touched, error, warning } }) => (
  <TextField
    select
    fullWidth
    value={input.value}
    onChange={(event) => input.onChange(event.target.value)}
    className={classes.selectEmpty}
    SelectProps={{
      native: true
    }}
    helperText={touched && error}
    error={touched && error ? true : false}
  >
    <option value="">{placeholder}</option>
    {
      elements.map(element =>
        <option key={element.id} value={element.id}>{element[criteria]}</option>)
    }
  </TextField>
);

export default withStyles(styles)(RenderSelectField);
