import React from 'react';

import { Button } from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

const styles = {
  container: {
    marginTop: 12
  },
  buttonNext: {
    marginLeft: 6
  }
};

const StepControllers = ({ classes, step: { active, isFinish }, next }) => (
  <div className={classes.container}>
    <Button disabled={active === 0}>
      atras
    </Button>
    <Button
      variant="outlined"
      color="secondary"
      className={classes.buttonNext}
      onClick={next}
    >
      siguiente
    </Button>
  </div>
);

StepControllers.defaultProps = {
  step: {
    active: 0
  }
};

export default withStyles(styles)(StepControllers);
