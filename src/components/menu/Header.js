import React from 'react';
import PropTypes from 'prop-types';

import { Paper, Typography } from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

const styles = {
  container: {
    backgroundColor: '#233D7B',
    padding: 12,
    color: '#FFFFFF'
  },
  title: {
    color: '#FFFFFF'
  }
};

const Header = ({ classes, roleName }) => (
  <Paper square className={classes.container}>
    <Typography variant="h5" color="inherit">
      Menú
    </Typography>
    <span className="d-inline-block text-truncate">
      {roleName}
    </span>
  </Paper>
);

Header.defaultProps = {
  roleName: 'require the field {roleName}'
};

Header.propTypes = {
  roleName: PropTypes.string.isRequired,
  classes: PropTypes.object
};

export default withStyles(styles)(Header);
