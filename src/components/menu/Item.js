
import React from 'react';
import PropTypes from 'prop-types';

/* components */
import { ListItem, ListItemText, Collapse } from '@material-ui/core';

/* icons */
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

/* containers */
import ItemMenu from '../../containers/commons/ItemMenu';

const Item = ({ onClick, isOpen, title, subtitle, options, level }) => (
  <Collapse in={level === 0 ? true : isOpen}>
    <ListItem button onClick={onClick}>
      <ListItemText
        style={{ paddingLeft: level * 12 }}
        primary={title}
        secondary={subtitle}
      />
      {options.length !== 0 && (isOpen ? <ExpandLess/> : <ExpandMore/>)}
    </ListItem>
    {
      options.map((option) => <ItemMenu key={option.id} {...option} level={level + 1} isOpen={isOpen}/>)
    }
  </Collapse>
);

Item.defaultProps = {
  onClick: () => { console.warn("required function {onClick}"); },
  isOpen: false,
  title: 'required the field {title}',
  options: [],
  level: 0
};

Item.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
  options: PropTypes.array.isRequired,
  level: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired
};

export default Item;
