import React from 'react';
import PropTypes from 'prop-types';

import {
  Drawer
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

import ItemMenu from '../../containers/commons/ItemMenu';

import Header from './Header';

const styles = {
  container: {
    minWidth: 250,
    maxWidth: 250,
    display: 'flex',
    flexDirection: 'column',
    height: '100%'
  },
  header: {
  },
  body: {
    flex: 1
  },
  footer: {
    backgroundColor: '#212121',
    color: '#BDBDBD',
    cursor: 'pointer',
    padding: 12,
    '&:hover': {
      color: '#F5F5F5'
    }
  }
};

const Menu = ({ classes, role, drawer, onClose, options }) => (
  <Drawer open={drawer} onClose={onClose}>
    <div className={classes.container}>
      <Header roleName={role}/>
      <div className={classes.body}>
        {
          options.map((option) =>
            <ItemMenu key={option.id} {...option}/>)
        }
      </div>
      <div className={classes.footer}>
        <span>
          SGC APP
        </span>
      </div>
    </div>
  </Drawer>
);

Menu.defaultProps = {
  role: 'required the field {role}',
  drawer: false,
  onClose: () => { console.warn('required the function {onClose}'); },
  options: []
};

Menu.propTypes = {
  role: PropTypes.string.isRequired,
  drawer: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  options: PropTypes.array.isRequired
};

export default withStyles(styles)(Menu);
