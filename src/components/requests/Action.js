import React from 'react';

import {
  Typography,
  IconButton
} from '@material-ui/core';

import DeleteIcon from '@material-ui/icons/Delete';

import { withStyles } from '@material-ui/core/styles';

const styles = {
  container: {
    padding: 12,
    cursor: 'pointer',
    display: 'flex',
    flexDirection: 'row',
    '&:hover': {
      backgroundColor: '#eeeeee'
    }
  },
  info: {
    flex: 1
  },
  icons: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
};

const Action = ({ created_at, action, scheduled_date, classes, description, responsible: { fullname }, onDelete }) => (
  <div className={classes.container}>
    <div className={classes.info}>
      <Typography>
        <strong>Fecha de creación:</strong> {created_at}
      </Typography>
      <Typography>
        <strong>Acción:</strong> {action}
      </Typography>
      <Typography>
        <strong>Descripción:</strong> {description}
      </Typography>
      <Typography>
        <strong>Encargado de realizar la acción:</strong> {fullname}
      </Typography>
      <Typography>
        <strong>Fecha programada:</strong> {scheduled_date}
      </Typography>
    </div>
    <div className={classes.icons}>
      <IconButton className={classes.button} aria-label="Delete" onClick={onDelete}>
        <DeleteIcon/>
      </IconButton>
    </div>
  </div>
);

export default withStyles(styles)(Action);
