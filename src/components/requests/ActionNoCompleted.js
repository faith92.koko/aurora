import React from 'react';
import moment from 'moment';

import {
  Typography
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

const styles = {
  container: {
    padding: 12,
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: '#eeeeee'
    }
  }
};

const ActionNoCompleted = ({ action, description, scheduled_date, classes, onClick }) => (
  <div className={classes.container} onClick={onClick}>
    <Typography>
      <strong>Fecha programada: </strong>
      {moment(scheduled_date).format("DD/MM/YYYY")}
      <strong style={{ color: 'red' }}> {moment().diff(scheduled_date, 'days') > 0 && 'Retrasado'}</strong>
    </Typography>
    <Typography>
      <strong>Action: </strong>{action}
    </Typography>
    <Typography>
      <strong>Descripción: </strong>{description}
    </Typography>
  </div>
);

export default withStyles(styles)(ActionNoCompleted);
