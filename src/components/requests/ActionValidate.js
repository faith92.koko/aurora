import React from 'react';

import {
  Typography
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

const styles = {
  container: {
    padding: 12,
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: '#eeeeee'
    }
  }
};

const ActionValidate = ({ folio, description, created_at, classes, action, go }) => (
  <div className={classes.container} onClick={go}>
    <Typography>
      <strong>Fecha de creación: </strong>{created_at}
    </Typography>
    <Typography>
      <strong>Folio: </strong>{folio}
    </Typography>
    <Typography>
      <strong>Descripción: </strong>{description}
    </Typography>
    <Typography>
      <strong>Acciones por evaluar: </strong>{action.length}
    </Typography>
  </div>
);

export default withStyles(styles)(ActionValidate);
