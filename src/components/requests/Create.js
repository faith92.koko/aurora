import React from 'react';

import {
  Step,
  Stepper,
  StepLabel,
  StepContent,
  Paper,
  Typography
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

/** components */
import General from '../../containers/requests/steps/General';
import Type from '../../containers/requests/steps/Type';
import ResponsibleArea from '../../containers/requests/steps/ResponsibleArea';
import Request from '../../containers/requests/steps/Request';
import Resume from '../../containers/requests/steps/Resume';
import Save from '../../containers/requests/steps/Save';

const styles = {
  header: {
    paddingLeft: 24,
    paddingTop: 24,
    paddingRight: 24
  }
};

const Create = ({ classes, step: { active } }) => (
  <div className="container">
    <div className="row justify-content-md-center">
      <div className="col-12 col-sm-12 col-md-10 col-lg-10">
        <Paper square>
          <Typography variant="h5" className={classes.header}>
            Nueva no conformidad
          </Typography>
          <Stepper activeStep={active} orientation="vertical">
            <Step>
              <StepLabel>¿De donde proviene la no conformidad?</StepLabel>
              <StepContent>
                <General/>
              </StepContent>
            </Step>
            <Step>
              <StepLabel>¿En donde se generó la no conformidad?</StepLabel>
              <StepContent>
                <Type/>
              </StepContent>
            </Step>
            <Step>
              <StepLabel>¿Quien es área responsable?</StepLabel>
              <StepContent>
                <ResponsibleArea/>
              </StepContent>
            </Step>
            <Step>
              <StepLabel>Información de la solicitud</StepLabel>
              <StepContent>
                <Request/>
              </StepContent>
            </Step>
            <Step>
              <StepLabel>Revise la información</StepLabel>
              <StepContent>
                <Resume/>
              </StepContent>
            </Step>
            <Step>
              <StepLabel>Guardar</StepLabel>
              <StepContent>
                <Save/>
              </StepContent>
            </Step>
          </Stepper>
        </Paper>
      </div>
    </div>
  </div>
);

Create.defaultProps = {
  step: {
    active: 0
  }
};

export default withStyles(styles)(Create);
