import React from 'react';

import {
  Typography
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

const styles = {
  container: {
    padding: 12,
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: '#eeeeee'
    }
  }
};

const Item = ({ classes, folio, description, onClick, request_type: { name } }) => (
  <div className={classes.container} onClick={onClick}>
    <Typography>
      <strong>Folio: </strong>{folio}
    </Typography>
    <Typography>
      <strong>Tipo: </strong>{name}
    </Typography>
    <Typography>
      <strong>Descripción: </strong>{description}
    </Typography>
  </div>
);

export default withStyles(styles)(Item);
