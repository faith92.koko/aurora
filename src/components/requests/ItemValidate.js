import React from 'react';

import {
  Paper,
  Typography,
  Divider
} from '@material-ui/core';

import {
  withStyles,
  Button
} from '@material-ui/core';

const styles = {
  container: {
    padding: 12,
    marginTop: 6
  },
  label: {
    padding: 12
  },
  buttons: {
    padding: 12,
    display: 'flex',
    justifyContent: 'flex-end'
  }
};

const ItemValidate = ({ action, description, results, classes, image, approve, id }) => (
  <div className="col-12 col-sm-12 col-md-8 col-lg-8">
    <Paper square>
      <div className={classes.container}>
        <Typography>
          <strong>Acción: </strong>{action}
        </Typography>
        <Typography>
          <strong>Descripción: </strong>{description}
        </Typography>
        <Typography>
          <strong>Resultados: </strong>{results}
        </Typography>
      </div>
      <Divider/>
      <Typography className={classes.label}>Evidencia: </Typography>
      <img alt="evidencia" src={image.url} width='100%'/>
      <Divider/>
      <div className={classes.buttons}>
        <Button variant="outlined" color="primary" onClick={() => { approve(id); }}>
          aprobar
        </Button>
      </div>
    </Paper>
  </div>
);

export default withStyles(styles)(ItemValidate);
