import React from 'react';

import {
  Paper,
  Divider,
  Typography,
  Button
} from '@material-ui/core';

import { Field } from 'redux-form';

import { withStyles } from '@material-ui/core/styles';

/** components */
import RenderField from '../commons/RenderField';
import RenderSelectField from '../commons/RenderSelectField';

const styles = {
  modal: {
    position: 'fixed',
    zIndex: 1300,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    padding: 12
  },
  buttons: {
    padding: 12,
    display: 'flex',
    justifyContent: 'flex-end'
  }
};

const Modal = ({ classes, handleSubmit, users, modal: { isOpen }, onToggle }) => (
  <div>
    {
      isOpen && (
        <div className={classes.modal}>
          <div className="container">
            <div className="row justify-content-md-center">
              <div className="col-10 col-sm-12 col-md-10 col-lg-8">
                <Paper square>
                  <form onSubmit={handleSubmit}>
                    <div className={classes.container}>
                      <Typography variant="h5">
                        Nueva acción correctiva
                      </Typography>
                      <Typography>
                        Capture una nueva acción correctiva.
                      </Typography>
                    </div>
                    <Divider/>
                    <div className={classes.container}>
                      <Field
                        name="action"
                        component={RenderField}
                        type="text"
                        label="Acción"
                        hint = "Introduzca la acción"
                      />
                      <Field
                        name="description"
                        component={RenderField}
                        type="text"
                        label="Descripción"
                        hint = "Introduzca la descripción de la acción"
                        multiline
                        rows="3"
                      />
                      <Field
                        name="responsible_user"
                        component={RenderSelectField}
                        elements={users.filter((user) => user.role_id === 4 || user.role_id === 3)}
                        criteria="fullname"
                        placeholder="¿Quién es el responsable de la acción?"
                      />
                      <Field
                        name="scheduled_date"
                        component={RenderField}
                        type="date"
                        label="Fecha programada"
                      />
                    </div>
                    <Divider/>
                    <div className={classes.buttons}>
                      <Button style={{ marginRight: 6 }} onClick={onToggle}>cancelar</Button>
                      <Button variant="outlined" color="primary" type="submit">crear</Button>
                    </div>
                  </form>
                </Paper>
              </div>
            </div>
          </div>
        </div>
      )
    }
  </div>
);

Modal.defaultProps = {
  users: [],
  modal: {
    isOpen: false
  }
};

export default withStyles(styles)(Modal);
