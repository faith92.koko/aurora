import React from 'react';
import { Field } from 'redux-form';

import {
  FormControlLabel,
  Radio
} from '@material-ui/core';

/** components */
import RenderRadioGroup from '../../commons/RenderRadioGroup';

const General = () => (
  <form>
    <Field name="origen" component={RenderRadioGroup}>
      <FormControlLabel value="sgc" control={<Radio/>} label="Sistema Gestión de Calidad"/>
      <FormControlLabel value="sga" control={<Radio/>} label="Sistema Gestión Ambiental"/>
    </Field>
  </form>
);

export default General;
