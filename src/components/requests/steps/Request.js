import React from 'react';
import { Field } from 'redux-form';

import { Button } from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

/** components */
import RenderField from '../../commons/RenderField';
import RenderSelectField from '../../commons/RenderSelectField';

const styles = {
  buttons: {
    marginTop: 12
  },
  buttonNext: {
    marginLeft: 6
  }
};

const Request = ({ handleSubmit, users, classes, back }) => (
  <div>
    <form onSubmit={handleSubmit}>
      <Field
        name="description"
        component={RenderField}
        type="text"
        label="Descripción"
        hint="Describa lo más preciso posible la no conformidad"
        multiline
        rows="3"
      />
      <Field
        name="responsable_for_corrective_actions"
        component={RenderSelectField}
        elements={users.filter((user) => user.role_id === 4 || user.role_id === 3)}
        criteria="fullname"
        placeholder="¿Quíen definirá las acciones correctivas?"
      />
      <Field
        name="responsable_for_verify_corrective_actions"
        component={RenderSelectField}
        elements={users.filter((user) => user.role_id === 4 || user.role_id === 3)}        criteria="fullname"
        placeholder="¿Quíen verificará el cumplimiento de las acciones?"
      />
      <div className={classes.buttons}>
        <Button onClick={back}>
          atras
        </Button>
        <Button
          variant="outlined"
          color="secondary"
          type="submit"
          className={classes.buttonNext}
        >
          continuar
        </Button>
      </div>
    </form>
  </div>
);

export default withStyles(styles)(Request);
