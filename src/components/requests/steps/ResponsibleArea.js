import React from 'react';
import { Field } from 'redux-form';

import {
  FormControlLabel,
  Radio,
  Button,
  Typography
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

/** components */
import RenderRadioGroup from '../../commons/RenderRadioGroup';

const styles = {
  buttons: {
    marginTop: 12
  },
  buttonNext: {
    marginLeft: 6
  },
  noitems: {
    color: '#757575'
  }
};

const Type = ({ classes, handleSubmit, pristine, back, areas }) => (
  <form onSubmit={handleSubmit}>
    {
      areas.length !== 0
        ? (
          <Field name="responsible_area_id" component={RenderRadioGroup}>
            {
              areas.map((area) => <FormControlLabel key={area.id} value={area.id.toString()} label={area.name} control={<Radio/>}/>)
            }
          </Field>
        )
        : (
          <Typography className={classes.noitems}>
            No puede continuar, solicite al administrador del sistema que configure las áreas con las que cuenta el instituto.
          </Typography>
        )
    }
    {
      areas.length !== 0 && (
        <div className={classes.buttons}>
          <Button onClick={back}>
            atras
          </Button>
          <Button
            variant="outlined"
            color="secondary"
            type="submit"
            className={classes.buttonNext}
            disabled={pristine}
          >
            continuar
          </Button>
        </div>
      )
    }
  </form>
);

export default withStyles(styles)(Type);
