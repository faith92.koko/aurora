import React from 'react';

import {
  Typography,
  Button
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

const styles = {
  text: {
    color: '#757575',
    fontSize: 16
  },
  buttons: {
    marginTop: 12
  },
  buttonNext: {
    marginLeft: 6
  }
};

const Resume = ({ data: { description, type, responsable_for_corrective_actions, responsable_for_verify_corrective_actions, origen, area }, classes, back, complete }) => (
  <div>
    <Typography variant="h5">
      <strong>Origen: </strong>
      <p className={classes.text}>
        {
          description === 'sga'
            ? 'Sistema Gestión Ambiental'
            : 'Sistema Gestión de Calidad'
        }
      </p>
    </Typography>
    <Typography variant="h5">
      <strong>Área responsable: </strong> <p className={classes.text}>{area}</p>
    </Typography>
    <Typography variant="h5">
      <strong>Descripción: </strong> <p className={classes.text}>{description}</p>
    </Typography>
    <Typography variant="h5">
      <strong>Tipo: </strong> <p className={classes.text}>{type}</p>
    </Typography>
    <Typography variant="h5">
      <strong>Persona encarga de escribir las acciones correctivas: </strong> <p className={classes.text}>{responsable_for_corrective_actions}</p>
    </Typography>
    <Typography variant="h5">
      <strong>Persona encargada de verificar las acciones correctivas: </strong> <p className={classes.text}>{responsable_for_verify_corrective_actions}</p>
    </Typography>
    <div className={classes.buttons}>
      <Button onClick={back}>
        atras
      </Button>
      <Button
        variant="outlined"
        color="secondary"
        className={classes.buttonNext}
        onClick={complete}
      >
        continuar
      </Button>
    </div>
  </div>
);

export default withStyles(styles)(Resume);
