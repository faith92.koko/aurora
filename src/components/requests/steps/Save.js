import React from 'react';

import { Button } from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

const styles = {
  buttons: {
    marginTop: 12
  },
  buttonNext: {
    marginLeft: 6
  }
};

const Save = ({ classes, back, save }) => (
  <div>
    <Button onClick={back}>
        atras
    </Button>
    <Button
      variant="outlined"
      color="secondary"
      type="submit"
      className={classes.buttonNext}
      onClick={save}
    >
      salvar la solicitud
    </Button>
  </div>
);

export default withStyles(styles)(Save);
