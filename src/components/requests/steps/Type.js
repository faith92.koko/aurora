import React from 'react';
import { Field } from 'redux-form';

import {
  FormControlLabel,
  Radio,
  Button
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

/** components */
import RenderRadioGroup from '../../commons/RenderRadioGroup';

/** static data */
import requestType from '../../../staticdata/requestTypes';

const styles = {
  buttons: {
    marginTop: 12
  },
  buttonNext: {
    marginLeft: 6
  }
};

const Type = ({ classes, handleSubmit, pristine, back }) => (
  <form onSubmit={handleSubmit}>
    <Field name="request_type_id" component={RenderRadioGroup}>
      {
        requestType.map((type) => <FormControlLabel key={type.value} {...type} control={<Radio/>}/>)
      }
    </Field>
    <div className={classes.buttons}>
      <Button onClick={back}>
        atras
      </Button>
      <Button
        variant="outlined"
        color="secondary"
        type="submit"
        className={classes.buttonNext}
        disabled={pristine}
      >
        continuar
      </Button>
    </div>
  </form>
);

export default withStyles(styles)(Type);
