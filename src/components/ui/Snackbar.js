import React from 'react';

import { Snackbar } from '@material-ui/core';

const SnackbarComponent = ({ closeSnackbar, snackbar: { isOpen, message } }) => (
  <Snackbar
    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
    open={isOpen}
    message={<span>{message}</span>}
    autoHideDuration={4000}
    onClose={closeSnackbar}
  />
);

export default SnackbarComponent;
