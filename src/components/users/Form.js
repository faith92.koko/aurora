import React from 'react';

import { Field } from 'redux-form';

import {
  Paper,
  Typography,
  Divider,
  Button
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

/** fields */
import RenderField from '../commons/RenderField';
import RenderSelectField from '../commons/RenderSelectField';

/** static data */
import roles from '../../staticdata/roles';

/** normalizers */
const upper = value => value && value.toUpperCase();

const styles = {
  wrap: {
    padding: 12
  }
};

const Form = ({ classes, handleSubmit, initialValues, role_id, areas }) => (
  <div className="container">
    <div className="row justify-content-md-center">
      <div className="col-12 col-sm-12 col-md-8 col-lg-6">
        <Paper square>
          <div className={classes.wrap}>
            <Typography variant="h6">
              {initialValues ? 'Modificar usuario' : 'Nuevo usuario'}
            </Typography>
            <Typography variant="subtitle2">
              {initialValues ? 'Modifique la información del usuario' : 'Rellene el formulario para crear un nuevo usuario.'}
            </Typography>
          </div>
          <Divider/>
          <div className={classes.wrap}>
            <form onSubmit={handleSubmit}>
              <Field
                name="fullname"
                component={RenderField}
                type="text"
                label="Nombre completo"
                hint = "Introduzca el nombre completo"
              />
              <Field
                name="email"
                component={RenderField}
                type="email"
                label="Correo electrónico"
                hint = "Introduzca su correo electrónico"
              />
              <Field
                name="rfc"
                component={RenderField}
                type="text"
                label="Registro Federal de Contribuyentes"
                hint = "Introduzca el RFC"
                normalize={upper}
              />
              <Field
                name="curp"
                component={RenderField}
                type="text"
                label="CURP"
                hint = "Introduzca la CURP"
                normalize={upper}
              />
              <Field
                name="password"
                component={RenderField}
                type="password"
                label="Contraseña"
                hint = {initialValues ? "Inserte una nueva contraseña" : "Introduzca la contraseña para este usuario"}
              />
              <Field
                name="role_id"
                component={RenderSelectField}
                elements={roles}
                criteria="name"
                placeholder="Seleccione el rol para el usuario."
                type="text"
              />
              {
                (role_id === '4' || (initialValues && initialValues.role_id === 4)) && (
                  <Field
                    name="area_id"
                    component={RenderSelectField}
                    elements={areas.filter((area) => area.responsible === null)}
                    criteria="name"
                    placeholder={initialValues ? initialValues.responsible.name : '¿En que área se encuentra?'}
                    type="text"
                  />
                )
              }
              <div className="d-flex justify-content-end" style={{ marginTop: 12 }}>
                <Button
                  variant="outlined"
                  color="secondary"
                  className={classes.button}
                  type="submit"
                >
                  {initialValues ? 'actualizar' : 'registrar'}
                </Button>
              </div>
            </form>
          </div>
        </Paper>
      </div>
    </div>
  </div>
);


Form.defaultProps = {
  areas: []
};

export default withStyles(styles)(Form);
