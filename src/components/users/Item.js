import React from 'react';

import {
  Switch,
  IconButton
} from '@material-ui/core';

import EditIcon from '@material-ui/icons/Edit';

import { withStyles } from '@material-ui/core/styles';

const styles = {
  container: {
    padding: 12,
    cursor: 'pointer',
    display: 'flex',
    '&:hover': {
      backgroundColor: '#f5f5f5'
    }
  },
  info: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column'
  },
  created_at: {
    color: '#9E9E9E',
    fontSize: 12
  },
  rfc: {
    color: '#616161'
  },
  actions: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
};

const Item = ({ classes, fullname, rfc, created_at, active, edit, onToggleUser, id, user }) => (
  <div className={classes.container}>
    <div className={classes.info}>
      <span className={classes.created_at}>
        Fecha de creación: {created_at}
      </span>
      <span>
        Nombre: {fullname}
      </span>
      <span className={classes.rfc}>
        RFC: {rfc}
      </span>
    </div>
    <div className={classes.actions}>
      <Switch checked={active} onChange={onToggleUser} disabled={id === user.id}/>
      <IconButton onClick={edit} disabled={!active}>
        <EditIcon/>
      </IconButton>
    </div>
  </div>
);

export default withStyles(styles)(Item);
