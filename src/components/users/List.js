import React from 'react';

import {
  Paper,
  Typography,
  Divider
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';

/** components */
import Item from '../../containers/users/Item';

const styles = {
  wraper: {
    padding: 12
  },
  bodylist: {
    paddingTop: 6,
    paddingBottom: 6
  }
};

const List = ({ classes, users: { data, isFetching, error } }) => (
  <div className="container">
    <div className="row justify-content-md-center">
      <div className="col-12 col-sm-12 col-md-8 col-lg-6">
        <Paper square>
          <div className={classes.wraper}>
            <Typography variant="h5">
              Usuarios
            </Typography>
            <Typography variant="subtitle2">
              Lista de usuarios.
            </Typography>
          </div>
          <Divider/>
          <div className={classes.bodylist}>
            {
              data.map((user) =>
                <Item key={user.id} {...user}/>)
            }
          </div>
        </Paper>
      </div>
    </div>
  </div>
);

export default withStyles(styles)(List);
