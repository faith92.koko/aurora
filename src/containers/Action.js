import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';

/** component */
import Action from '../components/Action';

/** actions */
import { saveEvidence } from '../actions/requests';

const validate = (values) => {
  const errors = {};

  [ 'evidence', 'results', 'observations' ].forEach((field) => {
    if (!values[field]) {
      errors[field] = 'Este campo es requerido';
    }
  });

  return errors;
};

const onSubmit = (results, dispatch, { match: { params: { id } } }) => {
  dispatch(saveEvidence(results, id));
};

const mapStateToProps = (state, { match: { params: { id } } }) => ({
  action: state.resources.actionsincompleted.data.find((action) => action.id === parseInt(id))
});

const Container = connect(
  mapStateToProps
)(Action);

export default reduxForm({
  form: 'result',
  onSubmit,
  validate
})(Container);
