import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

/** components */
import Actions from '../components/Actions';

/** actions */
import { getRequestActionsPending } from '../actions/requests';
import { getUsers } from '../actions/users';

class Container extends PureComponent {
  componentDidMount () {
    const { getRequestActionsPending, getUsers } = this.props;
    getRequestActionsPending();
    getUsers();
  }

  render () {
    return <Actions {...this.props}/>;
  }
}

const mapStateToProps = (state) => ({
  requests: state.resources.requests
});

export default connect(
  mapStateToProps,
  { getRequestActionsPending, getUsers }
)(Container);
