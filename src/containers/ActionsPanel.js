import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

/** components */
import ActionsPanel from '../components/ActionsPanel';

/** actions */
import { toggleModal } from '../actions/ui';
import { getActions, closeActions } from '../actions/requests';

/** reducers */
import { getRequest } from '../reducers/resources/requests';

class Container extends PureComponent {
  componentDidMount () {
    const { getActions } = this.props;
    getActions();
  }

  render () {
    return <ActionsPanel {...this.props}/>;
  }
}

const mapStateToProps = (state, { match: { params: { id } } }) => ({
  request: getRequest(state.resources.requests, id),
  request_id: id,
  actions: state.resources.actions
});

const mapDispatchToProps = (dispatch, { match: { params: { id } } }) => ({
  newAction: () => { dispatch(toggleModal('newaction')); },
  getActions: () => { dispatch(getActions(id)); },
  closeActions: () => { dispatch(closeActions(id)); }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Container);
