import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

/** component */
import ActionsValidate from '../components/ActionsValidate';

/** actions */
import { getActionsToBeEvaluated } from '../actions/requests';

class Container extends PureComponent {
  componentDidMount () {
    const { getActions } = this.props;
    getActions();
  }

  render () {
    return <ActionsValidate {...this.props}/>;
  }
}

const mapStateToProps = (state) => ({
  actions: state.resources.actionsvalidate
});

const mapDispatchToProps = (dispatch) => ({
  getActions: () => { dispatch(getActionsToBeEvaluated()); },
  go: (id) => { dispatch(push(`/acciones/evaluar/${id}`)); }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Container);
