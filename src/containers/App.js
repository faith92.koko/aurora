import React, { Component } from 'react';
import { connect } from 'react-redux';

/** components */
import App from '../components/App';

/** actions */
import { initApp } from '../actions/core';

class Container extends Component {
  componentDidMount () {
    /** init app */
    const { initApp } = this.props;
    initApp();
  }

  render () {
    const props = this.props;
    return <App {...props}/>;
  }
}

const mapStateToProps = (state) => ({
  app: state.core.app
});

export default connect(
  mapStateToProps,
  { initApp }
)(Container);
