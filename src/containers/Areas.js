import React, { Component } from 'react';
import { connect } from 'react-redux';

/** component */
import Areas from '../components/Areas';

/** actions */
import { getAreas, toggleArea } from '../actions/areas';

class Container extends Component {
  componentDidMount () {
    const { getAreas } = this.props;
    getAreas();
  }

  render () {
    return <Areas {...this.props}/>;
  }
}

const mapStateToProps = (state) => ({
  areas: state.resources.areas
});

export default connect(
  mapStateToProps,
  { getAreas, toggleArea }
)(Container);
