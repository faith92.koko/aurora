import { connect } from 'react-redux';

/** components */
import Header from '../components/Header';

/** actions */
import { logout } from '../actions/auth';
import { toggleDrawer } from '../actions/ui';

const mapStateToProps = (state) => ({
  isAuth: state.auth.isAuth
});

export default connect(
  mapStateToProps,
  { logout, toggle: toggleDrawer }
)(Header);
