import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

/** components */
import Login from '../components/Login';

/** actions */
import { startAuth } from '../actions/auth';

/** reducer */
import { isLogging, getErrors } from '../reducers/auth';

const validate = (values) => {
  const errors = {};

  [ 'email', 'password' ].forEach((field) => {
    if (!values[field]) {
      errors[field] = 'Este campo es requerido';
    }
  });

  return errors;
};

const onSubmit = (auth, dispatch) => {
  dispatch(startAuth(auth));
};

const mapStateToProps = (state) => ({
  isLogging: isLogging(state.auth),
  errors: getErrors(state.auth)
});

const Container = connect(
  mapStateToProps
)(Login);

export default reduxForm({
  form: 'authForm',
  onSubmit,
  validate
})(Container);
