import React from 'react';
import { connect } from 'react-redux';

/** components */
import Menu from '../components/menu/Menu';

/** reducers */
import { getRole } from '../reducers/auth';

/** actions */
import { toggleDrawer } from '../actions/ui';

const Container = ({ isAuth, ...props }) =>
  isAuth ? <Menu {...props}/> : <div/>;

const mapStateToProps = (state) => {
  const isAuth = state.auth.isAuth;
  return {
    isAuth,
    role: isAuth ? getRole(state.auth).name : null,
    drawer: state.ui.drawer.isOpen,
    options: state.ui.drawer.options
  };
};

export default connect(
  mapStateToProps,
  { onClose: toggleDrawer }
)(Container);
