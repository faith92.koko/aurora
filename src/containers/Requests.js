import React, { Component } from 'react';
import { connect } from 'react-redux';

/** components */
import Requests from '../components/Requests';

/** actions */
import { getRequests } from '../actions/requests';

/** reducers */

class Container extends Component {
  componentDidMount () {
    const { getRequests } = this.props;
    getRequests();
  }

  render () {
    return <Requests {...this.props}/>;
  }
}

const mapStateToProps = (state) => ({
  requests: state.resources.completedrequests.data
});

export default connect(
  mapStateToProps,
  { getRequests }
)(Container);
