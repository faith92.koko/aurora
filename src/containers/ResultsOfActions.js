import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

/** components */
import ResultsOfActions from '../components/ResultsOfActions';

/** actions */
import { getActionsIncompleted } from '../actions/requests';

class Container extends PureComponent {
  componentDidMount () {
    const { getActionsIncompleted } = this.props;
    getActionsIncompleted();
  }

  render () {
    return <ResultsOfActions {...this.props}/>;
  }
}

const mapStateToProps = (state) => ({
  actions: state.resources.actionsincompleted.data
});

export default connect(
  mapStateToProps,
  { getActionsIncompleted, push }
)(Container);
