import React, { Component } from 'react';
import { connect } from 'react-redux';

/** components */
import Resume from '../components/Resume';

/** actions */
import { getRequests } from '../actions/requests';
import { getAreas } from '../actions/areas';

class Container extends Component {
  componentDidMount () {
    const { getRequests,  getAreas } = this.props;
    getRequests();
    getAreas(true);
  }

  render () {
    return <Resume {...this.props}/>;
  }
}

const mapStateToProps = (state) => ({
  requests: state.resources.completedrequests.data,
  areas: state.resources.areas.data
});

export default connect(
  mapStateToProps,
  { getRequests,  getAreas }
)(Container);
