import { connect } from 'react-redux';

/** component */
import Validate from '../components/Validate';

/** actions */
import { approveAction, completedRequest } from '../actions/requests';

const mapStateToProps = (state, { match: { params: { id } } }) => ({
  actions: state.resources.actionsvalidate.data.find((request) => request.id === parseInt(id)).action
});

const mapDispatchToProps = (dispatch, { match: { params: { id } } }) => ({
  approveAction: (id) => { dispatch(approveAction(id)); },
  completed: () => { dispatch(completedRequest(id)); }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Validate);
