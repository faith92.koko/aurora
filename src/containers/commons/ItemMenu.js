import { connect } from 'react-redux';
import _ from 'lodash';

/** components */
import ItemMenu from '../../components/menu/Item';

/** actions */
import { toggleOptionMenu, openOption } from '../../actions/ui';

const mapDispatchToProps = (dispatch, { id, path, options }) => ({
  onClick: _.isUndefined(options)
    ? () => dispatch(openOption(path))
    : () => dispatch(toggleOptionMenu(id))
});

export default connect(
  null,
  mapDispatchToProps
)(ItemMenu);
