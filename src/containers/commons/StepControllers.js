import { connect } from 'react-redux';

/** component */
import StepControllers from '../../components/commons/StepControllers';

/** actions */
import { completeStep } from '../../actions/ui';

/** reducers */
import { getStep } from '../../reducers/ui/step';

const mapStateToProps = (state, ownProps) => ({
  step: getStep(state.ui.step, ownProps.name)
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  next: () => { dispatch(completeStep(ownProps.name), {}); }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StepControllers);
