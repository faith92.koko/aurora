import { connect } from 'react-redux';

/** components */
import Action from '../../components/requests/Action';

/** actions */
import { deleteAction } from '../../actions/requests';

const mapDispatchToProps = (dispatch, { id }) => ({
  onDelete: () => { dispatch(deleteAction(id)); }
});

export default connect(
  null,
  mapDispatchToProps
)(Action);
