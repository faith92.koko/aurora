import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

/** components */
import Create from '../../components/requests/Create';

/** actions */
import { registerStep } from '../../actions/ui';

/** reducers */
import { getStep } from '../../reducers/ui/step';
import { getUsers } from '../../actions/users';

class Container extends PureComponent {

  componentDidMount () {
    const { registerStep, getUsers } = this.props;
    registerStep('newrequest', 6);
    getUsers();
  }

  render () {
    return <Create {...this.props}/>;
  }
}

const mapStateToProps = (state) => ({
  step: getStep(state.ui.step, 'newrequest')
});

export default connect(
  mapStateToProps,
  { registerStep, getUsers }
)(Container);
