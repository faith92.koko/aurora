import { connect } from 'react-redux';
import { push } from 'connected-react-router';

/** components */
import Item from '../../components/requests/Item';

const mapDispatchToProps = (dispatch, { id }) => ({
  onClick: () => {
    dispatch(push(`/acciones/capturar/${id}`));
  }
});

export default connect(
  null,
  mapDispatchToProps
)(Item);
