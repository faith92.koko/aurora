import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

/** components */
import Modal from '../../components/requests/Modal';

/** actions */
import { registerModal, toggleModal } from '../../actions/ui';
import { addAction } from '../../actions/requests';

/** reducers */
import { getModal } from '../../reducers/ui/modals';

class Container extends PureComponent {
  componentDidMount () {
    const { register } = this.props;
    register();
  }

  render () {
    return <Modal {...this.props}/>;
  }
}

const validate = (values) => {
  const errors = {};

  [ 'action', 'description', 'scheduled_date', 'responsible_user' ].forEach((field) => {
    if (!values[field]) {
      errors[field] = 'Este campo es requerido';
    }
  });

  return errors;
};

const mapStateToProps = (state) => ({
  modal: getModal(state.ui.modals, 'newaction'),
  users: state.resources.users.data
});

const mapDispatchToProps = (dispatch) => ({
  register: () => { dispatch(registerModal('newaction')); },
  onToggle: () => { dispatch (toggleModal('newaction')); }
});

Container = connect(
  mapStateToProps,
  mapDispatchToProps
)(Container);

const onSubmit = (action, dispatch, { request_id }) => {
  dispatch(addAction(action, request_id ));
};

export default reduxForm({
  form: 'newaction',
  onSubmit,
  validate
})(Container);
