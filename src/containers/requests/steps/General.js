import React, { PureComponent } from 'react';
import { reduxForm } from 'redux-form';

/** components */
import General from '../../../components/requests/steps/General';

/** actions */
import { completeStep } from '../../../actions/ui';

class Container extends PureComponent {
  constructor () {
    super();
    this.state = {
      completed: false
    };
  }

  componentDidUpdate () {
    const { dirty, valid, handleSubmit } = this.props;
    const { completed } = this.state;

    if (dirty && valid && !completed) {
      handleSubmit();
      this.setState({
        completed: true
      });
    }
  }

  render () {
    return <General {...this.props}/>;
  }
}

const validate = (values) => {
  const errors = {};
  [ 'origen' ].forEach((field) => {
    if (!values[field]) {
      errors[field] = 'Este campo es requerido';
    }
  });

  return errors;
};

const onSubmit = (data, dispatch) => {
  dispatch(completeStep("newrequest", data));
};

export default reduxForm({
  form: 'general',
  onSubmit,
  validate
})(Container);

