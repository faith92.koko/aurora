import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

/** component */
import Request from '../../../components/requests/steps/Request';

/** actions */
import { completeStep, backStep } from '../../../actions/ui';

const validate = (values) => {
  const errors = {};

  [ 'description', 'responsable_for_corrective_actions', 'responsable_for_verify_corrective_actions' ].forEach((field) => {
    if (!values[field]) {
      return errors[field] = 'Este campo es requerido';
    }
  });

  return errors;
};

const onSubmit = (data, dispatch) => {
  dispatch(completeStep('newrequest', data));
};

const mapStateToProps = (state) => ({
  users: state.resources.users.data
});

const mapDispatchToProps = (dispatch) => ({
  back: () => { dispatch(backStep('newrequest')); }
});

const Container = connect(
  mapStateToProps,
  mapDispatchToProps
)(Request);

export default reduxForm({
  form: 'request',
  onSubmit,
  validate
})(Container);
