import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

/** component */
import ResponsibleArea from '../../../components/requests/steps/ResponsibleArea';

/** actions */
import { completeStep, backStep } from '../../../actions/ui';
import { getAreas } from '../../../actions/areas';

class Container extends Component {
  componentDidMount () {
    const { getAreas } = this.props;
    getAreas();
  }

  render () {
    return <ResponsibleArea {...this.props}/>;
  }
}

const validate = (values) => {
  const errors = {};

  [ 'responsible_area_id' ].forEach(field => {
    if (!values[field]) {
      errors[field] = "Se requiere que se especifique el área responsable";
    }
  });

  return errors;
};

const onSubmit = (data, dispatch) => {
  dispatch(completeStep('newrequest', data));
};

const mapStateToProps = (state) => ({
  areas: state.resources.areas.data
});

const mapDispatchToProps = (dispatch) => ({
  back: () => { dispatch(backStep('newrequest')); },
  getAreas: () => { dispatch(getAreas(true)); }
});

Container = connect(
  mapStateToProps,
  mapDispatchToProps
)(Container);

export default reduxForm({
  form: 'responsible_area',
  onSubmit,
  validate
})(Container);
