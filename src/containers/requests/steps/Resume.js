import { connect } from 'react-redux';

/** components */
import Resume from '../../../components/requests/steps/Resume';

/** actions */
import { completeStep, backStep } from '../../../actions/ui';

/** reducer */
import { getData } from '../../../reducers/ui/step';
import { getUser } from '../../../reducers/resources/users';
import { getArea } from '../../../reducers/resources/areas';

/** static data */
import requestType from '../../../staticdata/requestTypes';

const mapStateToProps = (state) => {
  let data = getData(state.ui.step, 'newrequest');
  const type = requestType.find((type) => type.value === data.request_type_id).label;

  data = {
    ...data,
    responsable_for_corrective_actions:
      getUser(state.resources.users, parseInt(data.responsable_for_corrective_actions))
        .fullname,
    responsable_for_verify_corrective_actions:
      getUser(state.resources.users, parseInt(data.responsable_for_verify_corrective_actions))
        .fullname,
    area: getArea(state.resources.areas, parseInt(data.responsible_area_id)).name,
    type
  };

  return {
    data
  };
};

const mapDispatchToProps = (dispatch) => ({
  back: () => { dispatch(backStep('newrequest')); },
  complete: () => { dispatch(completeStep('newrequest')); }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Resume);
