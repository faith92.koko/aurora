import { connect } from 'react-redux';

/** components */
import Save from '../../../components/requests/steps/Save';

/** actions */
import { backStep } from '../../../actions/ui';
import { saveRequest } from '../../../actions/requests';

const mapDispatchToProps = (dispatch) => ({
  back: () => { dispatch(backStep('newrequest')); },
  save: () => { dispatch(saveRequest()); }
});

export default connect(
  null,
  mapDispatchToProps
)(Save);
