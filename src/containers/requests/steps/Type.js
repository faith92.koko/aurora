import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

/** component */
import Type from '../../../components/requests/steps/Type';

/** actions */
import { completeStep, backStep } from '../../../actions/ui';

const validate = (values) => {
  const errors = {};

  [ 'request_type_id' ].forEach(field => {
    if (!values[field]) {
      errors[field] = "Se requiere que se especifique el origen";
    }
  });

  return errors;
};

const onSubmit = (data, dispatch) => {
  dispatch(completeStep('newrequest', data));
};

const mapDispatchToProps = (dispatch) => ({
  back: () => { dispatch(backStep('newrequest')); }
});

const Container = connect(
  null,
  mapDispatchToProps
)(Type);

export default reduxForm({
  form: 'type',
  onSubmit,
  validate
})(Container);
