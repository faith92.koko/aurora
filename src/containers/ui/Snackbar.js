import { connect } from 'react-redux';

/* component */
import Snackbar from '../../components/ui/Snackbar';

/* actions */
import { closeSnackbar } from '../../actions/ui';

const mapStateToProps = (state) => ({
  snackbar: state.ui.snackbar
});

export default connect(
  mapStateToProps,
  { closeSnackbar }
)(Snackbar);
