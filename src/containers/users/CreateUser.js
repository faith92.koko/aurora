import React, { PureComponent } from 'react';
import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';

/** component */
import Form from '../../components/users/Form';

/** actions */
import { createUser } from '../../actions/users';
import { getAreas } from '../../actions/areas';

class Container extends PureComponent {
  componentDidMount () {
    const { getAreas } = this.props;
    getAreas(true);
  }

  render () {
    return <Form {...this.props}/>;
  }
}

const validate = (values) => {
  const errors = {};

  [ 'fullname', 'email', 'rfc', 'curp', 'role_id' ].forEach((field) => {
    if (!values[field]) {
      errors[field] = 'Este campo es requerido';
    }
  });

  if (values.fullname) {
    if (values.fullname.length < 13) {
      errors.fullname = 'Mínimo son 13 letras para el nombre';
    } else if (values.fullname.length > 76) {
      errors.fullname = 'Máximo son 75 letras para el nombre';
    }
  }

  if (values.rfc) {
    if (values.rfc.length !== 13) {
      errors.rfc = 'El RFC esta compuesto por 13 caracteres.';
    }
  }

  if (values.curp) {
    if (values.curp.length !== 18) {
      errors.curp = 'La CURP esta compuesta por 18 caracteres.';
    }
  }

  if (values.role_id) {
    if (!values.area_id) {
      errors.area_id = 'Se requiere especificar el area';
    }
  }

  return errors;
};

const onSubmit = (user, dispatch) => {
  dispatch(createUser(user));
};

Container = reduxForm({
  form: 'formUser',
  onSubmit,
  validate
})(Container);

const selector = formValueSelector('formUser');

const mapStateToProps = (state) => ({
  role_id: selector(state, 'role_id'),
  areas: state.resources.areas.data
});

Container = connect(
  mapStateToProps,
  { getAreas }
)(Container);

export default Container;
