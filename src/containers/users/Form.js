import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';

/** component */
import Form from '../../components/users/Form';

/** actions */
import { createUser, updateUser } from '../../actions/users';

/** reducer */
import { getUser } from '../../reducers/resources/users';

const validate = (values) => {
  const errors = {};

  [ 'fullname', 'email', 'rfc', 'curp', 'role_id' ].forEach((field) => {
    if (!values[field]) {
      errors[field] = 'Este campo es requerido';
    }
  });

  if (values.fullname) {
    if (values.fullname.length < 13) {
      errors.fullname = 'Mínimo son 13 letras para el nombre';
    } else if (values.fullname.length > 76) {
      errors.fullname = 'Máximo son 75 letras para el nombre';
    }
  }

  if (values.rfc) {
    if (values.rfc.length !== 13) {
      errors.rfc = 'El RFC esta compuesto por 13 caracteres.';
    }
  }

  if (values.curp) {
    if (values.curp.length !== 18) {
      errors.curp = 'La CURP esta compuesta por 18 caracteres.';
    }
  }

  return errors;
};

const onSubmit = (user, dispatch, { initialValues }) => {
  if (initialValues) {
    dispatch(updateUser(user));
  } else {
    dispatch(createUser(user));
  }
};

const Container = reduxForm({
  form: 'formUser',
  onSubmit,
  validate
})(Form);

const mapStateToProps = (state, { match: { params: { id } } }) => ({
  initialValues: id && getUser(state.resources.users, parseInt(id))
});

export default connect(
  mapStateToProps
)(Container);
