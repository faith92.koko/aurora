import { connect } from 'react-redux';
import { push } from 'connected-react-router';

/** component */
import Item from '../../components/users/Item';

/** actions */
import { updateUser } from '../../actions/users';

const mapStateToProps = (state) => ({
  user: state.auth.user
});

const mapDispatchToProps = (dispatch, user) => ({
  edit: () => dispatch(push(`/usuarios/${user.id}`)),
  onToggleUser: () => dispatch(updateUser({ ...user, active: !user.active }))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Item);
