import React, { Component } from 'react';
import { connect } from 'react-redux';

/** component */
import List from '../../components/users/List';

/** actions */
import { getUsers } from '../../actions/users';

class Container extends Component {
  componentDidMount () {
    const { getUsers } = this.props;
    getUsers();
  }

  render () {
    return <List {...this.props}/>;
  }
}

const mapStateToProps = (state) => ({
  users: state.resources.users
});

export default connect(
  mapStateToProps,
  { getUsers }
)(Container);
