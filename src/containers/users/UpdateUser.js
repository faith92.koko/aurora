import React, { PureComponent } from 'react';
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';

/** component */
import Form from '../../components/users/Form';

/** actions */
import { updateUser } from '../../actions/users';
import { getAreas } from '../../actions/areas';

/** reducer */
import { getUser } from '../../reducers/resources/users';

class Container extends PureComponent {
  componentDidMount () {
    const { getAreas } = this.props;
    getAreas(true);
  }

  render () {
    return <Form {...this.props}/>;
  }
}

const validate = (values) => {
  const errors = {};

  [ 'fullname', 'email', 'rfc', 'curp', 'role_id' ].forEach((field) => {
    if (!values[field]) {
      errors[field] = 'Este campo es requerido';
    }
  });

  if (values.fullname) {
    if (values.fullname.length < 13) {
      errors.fullname = 'Mínimo son 13 letras para el nombre';
    } else if (values.fullname.length > 76) {
      errors.fullname = 'Máximo son 75 letras para el nombre';
    }
  }

  if (values.rfc) {
    if (values.rfc.length !== 13) {
      errors.rfc = 'El RFC esta compuesto por 13 caracteres.';
    }
  }

  if (values.curp) {
    if (values.curp.length !== 18) {
      errors.curp = 'La CURP esta compuesta por 18 caracteres.';
    }
  }

  if (values.role_id) {
    if (!values.area_id) {
      errors.area_id = 'Se requiere especificar el area';
    }
  }

  return errors;
};

const onSubmit = (user, dispatch) => {
  dispatch(updateUser(user));
};

Container = reduxForm({
  form: 'formUser',
  onSubmit,
  validate
})(Container);

const mapStateToProps = (state, { match: { params: { id } } }) => ({
  initialValues: id && getUser(state.resources.users, parseInt(id)),
  areas: state.resources.areas.data
});

export default connect(
  mapStateToProps,
  { getAreas }
)(Container);
