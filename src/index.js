import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';

import axios from 'axios';

import configureStore from './store/configureStore';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import history from './history';

/** importar bootstrap */
import 'bootstrap/dist/css/bootstrap.css';

/** importar estilos base */
import './index.css';

/** contenedores */
import Root from './containers/Root';

/** create store */
const store = configureStore();

/** crear el tema de la aplicación */
const theme = createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    primary: {
      light: '#233D7B',
      main: '#233D7B',
      dark: '#233D7B',
      contrastText: '#ffffff'
    },
    secondary: {
    light: '#233D7B',
    main: '#1A237E',
    dark: '#121858'
    }
  }
});

axios.defaults.baseURL = 'http://127.0.0.1:3333/api/v1';
axios.defaults.headers.post['Accept'] = 'application/json';

ReactDOM.render(
  <MuiThemeProvider theme = { theme }>
    <Root store={store} history={history}/>
  </MuiThemeProvider>,
  document.getElementById('root'));

serviceWorker.unregister();
