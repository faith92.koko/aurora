/** import actions */
import {
  START_AUTH,
  SUCCESS_AUTH,
  FAILURE_AUTH,
  CLEAR_AUTH
} from '../actions/auth';

const initialState = {
  isAuth: false,
  isLogging: false,
  error: null,
  user: null
};

const authReducer = (state = initialState, action) => {

  switch (action.type) {
    case START_AUTH:
      return {
        ...state,
        isLogging: true,
        error: null
      };

    case SUCCESS_AUTH:
      return {
        ...state,
        isLogging: false,
        isAuth: true,
        user: action.payload.user
      };

    case FAILURE_AUTH:
      return {
        ...state,
        isLogging: false,
        error: action.payload.error
      };

    case CLEAR_AUTH:
      return initialState;

    default:
      return state;
  }

};

export const isLogging = (state) =>
  state.isLogging;

export const getErrors = (state) =>
  state.error;

export const getRole = (state) =>
  state.user.role;

export default authReducer;
