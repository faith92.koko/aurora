import { OPEN_APP, UPDATE_APP_STATUS } from '../../actions/core';

const initialState = {
  isReady: false,
  status: null
};

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_APP:
      return {
        ...state,
        isReady: true,
        status: null
      };

    case UPDATE_APP_STATUS:
      return {
        isReady: false,
        status: action.payload.status
      };

    default:
      return state;
  }
};

export default appReducer;
