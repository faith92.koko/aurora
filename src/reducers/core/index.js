import { combineReducers } from 'redux';

import app from './app';

const coreReducer = combineReducers({
  app
});

export default coreReducer;
