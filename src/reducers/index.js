import { combineReducers } from 'redux';

import { connectRouter } from 'connected-react-router';
import { reducer as formReducer } from 'redux-form';

import history from '../history';

/** reducers */
import auth from './auth';
import ui from './ui';
import core from './core';
import resources from './resources';

const rootReducer = combineReducers({
  router: connectRouter(history),
  form: formReducer,
  auth,
  ui,
  core,
  resources
});

export default rootReducer;
