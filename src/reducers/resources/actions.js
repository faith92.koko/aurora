import {
  START_GET_ACTIONS,
  SUCCESS_GET_ACTIONS,
  ADD_ACTION,
  DELETE_ACTION
} from '../../actions/requests';

const initialValues = {
  isFetching: false,
  error: null,
  data: []
};

const actionsReducer = (state = initialValues, action) => {
  switch (action.type) {
    case START_GET_ACTIONS:
      return {
        ...state,
        error: null,
        isFetching: true
      };

    case SUCCESS_GET_ACTIONS:
      return {
        ...state,
        isFetching: false,
        data: action.payload.data
      };

    case ADD_ACTION:
      return {
        ...state,
        data: [
          ...state.data,
          action.payload.action
        ]
      };

    case DELETE_ACTION:
      return {
        ...state,
        data: state.data.filter((a) => a.id !== action.payload.id)
      };

    /** TODO: Failure get actions */

    default:
      return state;
  }
};

export default actionsReducer;
