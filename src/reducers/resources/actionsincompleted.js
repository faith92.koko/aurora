import {
  START_GET_ACTION_INCOMPLETED,
  SUCCESS_GET_ACTION_INCOMPLETED
} from '../../actions/requests';

const initialValues = {
  isFetching: false,
  data: [],
  error: null
};

const actionsIncompletedReducer = (state = initialValues, action) => {
  switch (action.type) {
    case START_GET_ACTION_INCOMPLETED:
      return {
        ...state,
        isFetching: true,
        error: null
      };

    case SUCCESS_GET_ACTION_INCOMPLETED:
      return {
        ...state,
        isFetching: false,
        data: action.payload.data
      };

    default:
      return state;
  }
};

export default actionsIncompletedReducer;
