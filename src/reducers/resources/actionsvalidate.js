import {
  START_GET_ACTION_VALIDATE,
  SUCCESS_GET_ACTION_VALIDATE,
  APPROVE_ACTION_VALIDATE
} from '../../actions/requests';

const initialValues = {
  isFetching: false,
  data: [],
  error: null
};

const actionsValidate = (state = initialValues, action) => {
  switch (action.type) {
    case START_GET_ACTION_VALIDATE:
      return {
        ...state,
        isFetching: true,
        error: null
      };

    case SUCCESS_GET_ACTION_VALIDATE:
      return {
        ...state,
        isFetching: false,
        data: action.payload.data
      };

    case APPROVE_ACTION_VALIDATE:
      return {
        ...state,
        data: state.data.map((d) => ({
          ...d,
          action: d.action.filter((c) => c.id !== action.payload.id)
        }))
      };

    default:
      return state;
  }
};

export default actionsValidate;
