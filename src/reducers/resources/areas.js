import {
  START_GET_AREAS,
  SUCCESS_GET_AREAS,
  TOGGLE_AREA
} from '../../actions/areas';

const initialState = {
  isFetching: false,
  data: [],
  error: null
};

const areasReducer = (state = initialState, action) => {
  switch (action.type) {
    case START_GET_AREAS:
      return {
        ...state,
        isFetching: true,
        error: null
      };

    case SUCCESS_GET_AREAS:
      return {
        ...state,
        isFetching: false,
        data: action.payload.data
      };

    case TOGGLE_AREA:
      return {
        ...state,
        data: state.data.map((area) => area.id === action.payload.id ? {
            ...area,
            active: !area.active
          } : area)
      };

    default:
      return state;
  }
};

export const getArea = (state, id) =>
  state.data.find((area) => area.id === id);


export default areasReducer;
