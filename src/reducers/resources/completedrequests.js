import {
  START_GET_REQUESTS,
  SUCCESS_GET_REQUESTS
} from '../../actions/requests';

const initialState = {
  isFetching: false,
  error: null,
  data: []
};

const completedRequests = (state = initialState, action) => {
  switch (action.type) {
    case START_GET_REQUESTS:
      return {
        ...state,
        isFetching: true,
        error: null
      };

    case SUCCESS_GET_REQUESTS:
      return {
        ...state,
        isFetching: false,
        data: action.payload.data
      };

    default:
      return state;
  }
};

export default completedRequests;
