import { combineReducers } from 'redux';

/** resource reducers */
import users from './users';
import areas from './areas';
import requests from './requests';
import actions from './actions';
import actionsincompleted from './actionsincompleted';
import actionsvalidate from './actionsvalidate';
import completedrequests from './completedrequests';

const resourceReducer = combineReducers({
  users,
  areas,
  requests,
  actions,
  actionsincompleted,
  actionsvalidate,
  completedrequests
});

export default resourceReducer;
