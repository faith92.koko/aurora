import {
  START_GET_ACTIONS_PENDING,
  SUCCESS_ACTIONS_PENDING
} from '../../actions/requests';

const initialState = {
  isFetching: false,
  error: null,
  data: []
};

const requestsReducer = (state = initialState, action) => {
  switch (action.type) {
    case START_GET_ACTIONS_PENDING:
      return {
        ...state,
        error: null,
        isFetching: true
      };

    case SUCCESS_ACTIONS_PENDING:
      return {
        ...state,
        isFetching: false,
        data: action.payload.requests
      };

    default:
      return state;
  }
};

export const getRequest = (state, id) =>
  state.data.find((request) => request.id === parseInt(id));

export default requestsReducer;
