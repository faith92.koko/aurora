import {
  START_GET_USERS,
  SUCCESS_GET_USERS,
  FAILURE_GET_USERS,
  SUCCESS_UPDATE_USER
} from '../../actions/users';

const initialState = {
  isFetching: false,
  error: null,
  data: []
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case START_GET_USERS:
      return {
        ...state,
        isFetching: true,
        error: null
      };

    case SUCCESS_GET_USERS:
      return {
        ...state,
        isFetching: false,
        data: action.payload.data
      };

    case FAILURE_GET_USERS:
      return {
        ...state,
        isFetching: false,
        error: action.payload.error
      };

    case SUCCESS_UPDATE_USER:
      return {
        ...state,
        data: state.data.map((user) => user.id === action.payload.data.id ? action.payload.data : user)
      };

    default:
      return state;
  }
};

/**
 * function to get a user by id
 *
 * @param {object} state
 * @param {integer} id
 *
 * @return {object}
 */
export const getUser = (state, userId) => {
  let user = state.data.find(({ id }) => id === userId);

  if (user && user.responsible) {
    user = {
      ...user,
      area_id: user.responsible.id
    };
  }

  return user;
};

export default userReducer;
