import { TOGGLE_DRAWER, INIT_DRAWER, TOGGLE_OPTION_MENU } from '../../actions/ui';

import menuData from '../../staticdata/menu';

/** reducer for each option */
const optionReducer = (state, action) => {
  switch (action.type) {
    case TOGGLE_OPTION_MENU:
      return {
        ...state,
        isOpen: state.id === action.payload.id ? !state.isOpen : false
      };

    case INIT_DRAWER:
    default:
      return {
        ...state,
        isOpen: false
      };
  }
};

const initialState = {
  isOpen: false,
  options: []
};

/** main reducer */
const DrawerReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_DRAWER:
      return {
        ...state,
        isOpen: !state.isOpen
      };

    case TOGGLE_OPTION_MENU:
      return {
        ...state,
        options: state.options.map((option) => optionReducer(option, action))
      };

    case INIT_DRAWER:
      const { id } = action.payload; /** get id user */
      return {
        ...state,
        options: menuData
          .filter((option) => option.roles.includes(id))
          .map((option) => ({
            ...option,
            options: option.options.filter((subOption) => subOption.roles.includes(id))
          }))
          .map((menu) => optionReducer(menu, action))
      };
    default:
      return state;
  }
};

export default DrawerReducer;
