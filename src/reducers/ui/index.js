import { combineReducers } from 'redux';

/** reducers */
import drawer from './drawer';
import snackbar from './snackbar';
import step from './step';
import modals from './modals';

const uiReducers = combineReducers({
  drawer,
  snackbar,
  step,
  modals
});

export default uiReducers;
