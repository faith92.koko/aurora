import {
  REGISTER_MODAL,
  TOGGLE_MODAL
} from '../../actions/ui';

const modalReducer = (state = { isOpen: false }, action) => {
  switch (action.type) {
    case REGISTER_MODAL:
      return {
        ...state,
        name: action.payload.modal
      };

    case TOGGLE_MODAL:
      return {
        ...state,
        isOpen: !state.isOpen
      };

    default:
      return state;
  }
};

const modalsReducers = (state = [], action) => {
  switch (action.type) {
    case REGISTER_MODAL:
      return [
        ...state,
        modalReducer(undefined, action)
      ];

    case TOGGLE_MODAL:
      return state
        .map((modal) => modal.name === action.payload.modal ? modalReducer(modal, action) : modal);

    default:
      return state;
  }
};

export const getModal = (state, name) =>
  state.find((modal) => modal.name === name);

export default modalsReducers;
