import {
  REGISTER_STEP,
  COMPLETE_STEP,
  BACK_STEP,
  CLEAR_STEP
} from '../../actions/ui';

const initialState = {
  name: '',
  isFinish: false,
  active: 0,
  completed: [],
  data: [],
  steps: 0
};

const step = (state = initialState, action) => {

  switch (action.type) {

    case REGISTER_STEP:
      return {
        ...state,
        ...action.payload
      };

    case COMPLETE_STEP:
      return {
        ...state,
        completed: state.completed.concat(state.active),
        active: state.active + 1,
        data: state.data.concat(action.payload.data)
      };

    case BACK_STEP:
      return {
        ...state,
        active: state.active > 0 ? state.active - 1 : 0,
        completed: state.completed.filter((index) => index !== state.active - 1),
        data: state.data.filter((s, i) => i !== state.active)
      };

    default:
      return state;

  }

};

const StepsReducer = (state = [], action) => {

  switch (action.type) {

    case REGISTER_STEP:
      return [
        ...state,
        step(undefined, action)
      ];

    case CLEAR_STEP:
      return state.filter(step => step.name !== action.payload.name);

    case COMPLETE_STEP:
    case BACK_STEP:
      return state.map(s => s.name === action.payload.name ? step(s, action) : s);

    default:
      return state;

  }

};

export const getSteps = (state, name) => {

  const step = state.find(step => step.name === name);

  if (step === undefined) {

    return undefined;

  } else if (step.data.length > 0) {

    return {
      ...step,
      data: step.data.reduce((previous, current, index, array) =>
        Object.assign({}, previous, current))
    };

  }

  return {
    ...step,
    data: {}
  };

};

/**
 * function to get a step
 *
 * @param {object} state state
 * @param {string} name name of the step
 */
export const getStep = (state, name) =>
  state.find((step) => step.name === name);

export const getData = (state, name) => {
  const step = getStep(state, name);
  if (step && step.data.length !== 0) {
    return step.data.reduce((fragment, recipient) => Object.assign({}, recipient, fragment));
  }
  return {};
};

export default StepsReducer;
