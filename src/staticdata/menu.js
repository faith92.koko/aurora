const data = [ {
  id: 'usuarios',
  title: 'Usuarios',
  subtitle: 'Gestione usuarios',
  roles: [ 1 ],
  options: [ {
    id: 'usuarios_1',
    title: 'Crear',
    subtitle: 'Cree cuentas',
    roles: [ 1 ],
    path: '/usuarios/crear'
  }, {
    id: 'usuarios_2',
    title: 'Usuario',
    subtitle: 'Todos los usuarios',
    roles: [ 1 ],
    path: '/usuarios'
  } ]
}, {
  id: 'noconformidades',
  title: 'No conformidades',
  subtitle: 'Información y operaciones',
  roles: [ 1, 3 ],
  options: [ {
    id: 'noconformidades_1',
    title: 'Nueva',
    subtitle: 'Capture una nueva no conformidad',
    roles: [ 3 ],
    path: '/solicitudes/no-conformidad'
  }, {
    id: 'noconformidades_5',
    title: 'Cerrar y elaborar informe',
    subtite: 'Cierre la solicitud y genere el informe final',
    roles: [ 1 ],
    path: '/solicitudes'
   }, {
    id: 'noconformidades_6',
    title: 'Resumen',
    subtite: 'Obtener el resumen general de los trámites',
    roles: [ 1 ],
    path: '/resumen'
  } ]
}, {
  id: 'accionescorrectivas',
  title: 'Acciones correctivas',
  subtitle: 'Capture y evalue',
  roles: [ 3, 4, 2 ],
  options: [ {
    id: 'accionescorrectivas_1',
    title: 'Nueva',
    subtitle: 'Capture acciones',
    roles: [ 3 ],
    path: '/acciones/capturar'
  }, {
    id: 'accionescorrectivas_2',
    title: 'Evaluar',
    subtitle: 'Evalue las acciones correctivas',
    path: '/acciones/evaluar',
    roles: [ 3, 2 ]
  }, {
    id: 'accionescorrectivas_3',
    title: 'Resultados',
    subtitle: 'Capture los resultados',
    path: '/acciones',
    roles: [ 3, 4 ]
  } ]
}, {
  id: 'informes',
  title: 'Informes',
  subtitle: 'Consulte los informes finales',
  roles: [ 2 ],
  options: [ {
    id: 'informes_1',
    title: 'Todos',
    subtitle: 'Visualice todos los informes',
    roles: [ 2 ]
  }, {
    id: 'informes_2',
    title: 'Por FOLIO',
    subtitle: 'Consulte por FOLIO',
    roles: [ 2 ]
  } ]
}, {
  id: 'configuraciones',
  title: 'Configuraciones',
  subtitle: 'Configure la información necesaria',
  roles: [ 1 ],
  options: [ {
    id: 'configuraciones_1',
    title: 'Areas Responsable',
    subtitle: 'Active las areas responsables',
    roles: [ 1 ],
    path: '/configuracion/areas-responsables'
  } ]
} ];

export default data;
