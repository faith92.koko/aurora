const RequestTypes = [ {
  value: '1',
  label: 'Queja del cliente'
}, {
  value: '2',
  label: 'Auditoría'
}, {
  value: '3',
  label: 'Incumplimiento de requisitos'
}, {
  value: '4',
  label: 'Eficacia / Desempeño de procesos'
}, {
  value: '5',
  label: 'Incumplimiento objetivos'
}, {
  value: '6',
  label: 'Otro'
} ];

export default RequestTypes;
