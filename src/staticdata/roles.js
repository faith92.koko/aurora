const roles = [ {
  id: 1,
  name: 'Administrador del sistema.'
}, {
  id: 2,
  name: 'Director/a del Instituto o Centro / Coordinador/a del SG en TecNM.'
}, {
  id: 3,
  name: 'Comité de Calidad / Ambiental / Subdirectores.'
}, {
  id: 4,
  name: 'Área responsable.'
} ];

export default roles;
